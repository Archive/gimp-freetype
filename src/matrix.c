/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GIMP FreeType Plug-in
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *                     Jens Lautenbacher <jtl@gimp.org>
 *                     Michael Natterer <mitch@gimp.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <glib.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "freetype-types.h"

#include "matrix.h"


Matrix2
matrix2_mult (Matrix2  m1,
	      Matrix2  m2)
{
  Matrix2 matrix;

  matrix.xx = m1.xx * m2.xx + m1.xy * m2.yx;
  matrix.xy = m1.xx * m2.xy + m1.xy * m2.yy;
  matrix.yx = m1.yx * m2.xx + m1.yy * m2.yx;
  matrix.yy = m1.yx * m2.xy + m1.yy * m2.yy;

  return matrix;
}

void
matrix2_transform_point (Matrix2    m,
			 FT_Vector *vec)
{
  FT_Matrix trans_matrix;

  trans_matrix.xx = (FT_Fixed) (m.xx * 65536.0);
  trans_matrix.xy = (FT_Fixed) (m.yx * 65536.0);
  trans_matrix.yx = (FT_Fixed) (m.xy * 65536.0);
  trans_matrix.yy = (FT_Fixed) (m.yy * 65536.0);

  FT_Vector_Transform (vec, &trans_matrix);
}
