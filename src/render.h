/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GIMP FreeType Plug-in
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *                     Jens Lautenbacher <jtl@gimp.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __RENDER_H__
#define __RENDER_H__


#define MAX_NUM_GLYPHS 256


typedef enum
{
  UPDATE_FACE      = 1 << 0,
  UPDATE_TEXT      = 1 << 1,
  UPDATE_SIZE      = 1 << 2,
  UPDATE_TRANSFORM = 1 << 3,
  UPDATE_RENDERING = 1 << 4,
  UPDATE_LAYOUT    = 1 << 5,
  UPDATE_FULL      = 0xffff
} RenderUpdate;


const FT_BBox * render_prepare       (gint32            image_ID,
				      gboolean          preview,
				      FT_Pos           *left,
				      FreeTypeVals     *vals,
				      FontFace         *font_face,
				      RenderUpdate      update);

void            render               (gint32            image_ID,
				      guchar           *preview_buffer,
				      gint              preview_width,
				      gint              preview_height,
				      gint              preview_x,
				      gint              preview_y,
				      FreeTypeVals     *vals,
				      FreeTypeRetVals  *retvals,
				      FontFace         *font_face);

void            init_glyphs          (void);
void            free_glyphs          (void);

void            blit_glyph_to_buffer (guchar           *buffer,
				      gint             buffer_width,
				      gint             buffer_height,
				      gint             preview_x,
				      gint             preview_y,
				      FT_Bitmap       *source,
				      gint             x_top,
				      gint             y_top,
				      gboolean         antialias);


#endif /* __RENDER_H__ */



