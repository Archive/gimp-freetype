/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GIMP FreeType Plug-in
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *                     Jens Lautenbacher <jtl@gimp.org>
 *
 * This plug-in contains code taken from the freetype2 library and
 * its demos, which is licensed under The FreeType Project LICENSE.
 *  Copyright 1996-2000 by
 *   David Turner, Robert Wilhelm, and Werner Lemberg
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <stdlib.h>
#include <time.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>

#include "freetype-types.h"
#include "freetype-intl.h"

#include "about.h"
#include "fontsel.h"
#include "main.h"
#include "render.h"


#define ANIMATION_STEPS  64
#define ANIMATION_SPEED  64


static gchar *authors[] =
{
  "Jens Lautenbacher",
  "Tor Lillqvist",
  "Michael Natterer",
  "Sven Neumann",
  "Andy Thomas"
};

static gint shuffle[G_N_ELEMENTS (authors)];

static GtkWidget    *about_dialog = NULL;
static FontFace     *font_face    = NULL;
static guchar       *buffer       = NULL;
static gint          timer        = 0;
static gint          frame        = 0;
static gint          preview_width;
static gint          preview_height;

static FreeTypeVals  about_vals =
{
  "",
  72,
  GIMP_UNIT_POINT,
  { 1.0, 0.0, 0.0, 1.0 },
  TRUE,
  TRUE,
  TRUE,
  FALSE,
  0,
  ""
};


/*  Private functions  */


static void
shuffle_array (vvoid)
{
  gint i;
  gint j;
  gint tmp;

  srand (time (NULL));

  for (i = 0; i < G_N_ELEMENTS (authors); i++)
    shuffle[i] = i;

  for (i = 0; i < G_N_ELEMENTS (authors); i++)
    {
      j = rand() % G_N_ELEMENTS (authors);
      if (i != j)
	{
	  tmp = shuffle[j];
	  shuffle[j] = shuffle[i];
	  shuffle[i] = tmp;
	}
    }
}

static gint
about_dialog_timer (gpointer data)
{
  GtkWidget     *preview = (GtkWidget *)data;
  RenderUpdate   update  = UPDATE_TRANSFORM;
  const FT_BBox *bbox;
  gint           offset_x;
  gint           offset_y;
  gint           i;

  frame += 1;

  if (frame == ANIMATION_STEPS)
    {
      frame = 0;
      g_snprintf (about_vals.text, BUF_SIZE, "GIMP");
      update |= UPDATE_TEXT;
    }

  if (frame * 2 == ANIMATION_STEPS)
    {
      g_snprintf (about_vals.text, BUF_SIZE, "FreeType");
      update |= UPDATE_TEXT;
    }

  about_vals.transform.yy = fabs (sin (frame * G_PI / ANIMATION_STEPS * 2));

  bbox = render_prepare (-1, FALSE, NULL, &about_vals, font_face, update);

  offset_x = ((bbox->xMax - bbox->xMin) - preview_width)        / 2;
  offset_y = ((bbox->yMax - bbox->yMin) - preview_height * 0.9) / 2;

  memset (buffer, 255, preview_width * preview_height);

  render (-1, buffer,
	  preview_width, preview_height, offset_x, offset_y,
	  &about_vals, NULL,
	  font_face);

  for (i = 0; i < preview_height; i++)
    gtk_preview_draw_row (GTK_PREVIEW (preview),
			  buffer + preview_width * i,
			  0, i, preview_width);

  gtk_widget_queue_draw (preview);

  return TRUE;
}

static void
about_dialog_destroy (void)
{
  if (timer)
    {
      g_source_remove (timer);
      timer = 0;
    }

  g_free (buffer);

  buffer                  = NULL;
  about_dialog            = NULL;
  frame                   = 0;
  about_vals.transform.yy = 1.0;
}


/*  Public functions  */

void
about_dialog_run (GtkWidget *parent)
{
  GtkWidget     *label;
  GtkWidget     *frame;
  GtkWidget     *vbox;
  GtkWidget     *preview;
  const FT_BBox *bbox;
  gchar         *text;
  gint           gimp_width;
  gint           gimp_height;
  gint           ft_width;
  gint           ft_height;
  gint           i;

  if (!about_dialog)
    {
      about_dialog = gimp_dialog_new (_("About GIMP FreeType"), "about",
                                      parent,
                                      GTK_DIALOG_MODAL |
                                      GTK_DIALOG_DESTROY_WITH_PARENT,
                                      NULL, NULL,

                                      NULL);

      gtk_window_set_resizable (GTK_WINDOW (about_dialog), FALSE);
      gtk_widget_set_events (about_dialog, GDK_BUTTON_PRESS_MASK);

      g_signal_connect (about_dialog, "response",
                        G_CALLBACK (gtk_widget_destroy),
                        NULL);
      g_signal_connect (about_dialog, "button_press_event",
                        G_CALLBACK (gtk_widget_destroy),
                        NULL);
      g_signal_connect (about_dialog, "destroy",
                        G_CALLBACK (about_dialog_destroy),
                        NULL);

      vbox = gtk_vbox_new (FALSE, 6);
      gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
      gtk_box_pack_start (GTK_BOX (GTK_DIALOG (about_dialog)->vbox),
                          vbox, TRUE, TRUE, 0);
      gtk_widget_show (vbox);

      frame = gtk_frame_new (NULL);
      gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
      gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);
      gtk_widget_show (frame);

      fontsel_get_selected_entry (&font_face);
      if (!font_face)
	{
	  label = gtk_label_new ("GIMP FreeType");
          gimp_label_set_attributes (GTK_LABEL (label),
                                     PANGO_ATTR_SCALE,  PANGO_SCALE_LARGE,
                                     PANGO_ATTR_WEIGHT, PANGO_WEIGHT_BOLD,
                                     -1);
	  gtk_misc_set_padding (GTK_MISC (label), 12, 12);
	  gtk_container_add (GTK_CONTAINER (frame), label);
	  gtk_widget_show (label);
	}
      else
	{
	  g_snprintf (about_vals.text, BUF_SIZE, "FreeType");
	  bbox = render_prepare (-1, FALSE, NULL,
				 &about_vals, font_face, UPDATE_FULL);
	  if (!bbox)
	    return;
	  ft_width  = bbox->xMax - bbox->xMin;
	  ft_height = bbox->yMax - bbox->yMin;

	  g_snprintf (about_vals.text, BUF_SIZE, "GIMP");
	  bbox = render_prepare (-1, FALSE, NULL,
				 &about_vals, font_face, UPDATE_FULL);
	  if (!bbox)
	    return;
	  gimp_width  = bbox->xMax - bbox->xMin;
	  gimp_height = bbox->yMax - bbox->yMin;

	  preview_width  = MAX (32, MAX (gimp_width,  ft_width)  * 1.2);
	  preview_height = MAX (16, MAX (gimp_height, ft_height) * 1.2);

	  preview = gtk_preview_new (GTK_PREVIEW_GRAYSCALE);
	  gtk_preview_size (GTK_PREVIEW (preview),
			    preview_width, preview_height);
	  gtk_container_add (GTK_CONTAINER (frame), preview);

	  buffer = g_new (guchar, preview_width * preview_height);
	  memset (buffer, 255, preview_width * preview_height);
	  for (i = 0; i < preview_height; i++)
	  gtk_preview_draw_row (GTK_PREVIEW (preview),
                                buffer + preview_width * i,
                                0, i, preview_width);
	  gtk_widget_queue_draw (preview);

	  timer = g_timeout_add (ANIMATION_SPEED,
                                 (GSourceFunc) about_dialog_timer,
                                 preview);
	  gtk_widget_show (preview);
	}

      text = g_strdup_printf (_("Version %s brought to you by"),
                              FREETYPE_VERSION);
      label = gtk_label_new (text);
      g_free (text);

      gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 6);
      gtk_widget_show (label);

      shuffle_array ();
      for (i = 0; i < G_N_ELEMENTS (authors); i++)
	{
	  label = gtk_label_new (authors[shuffle[i]]);
	  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	  gtk_widget_show (label);
	}
    }

  gimp_dialog_run (GIMP_DIALOG (about_dialog));
}
