/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GIMP FreeType Plug-in
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *                     Jens Lautenbacher <jtl@gimp.org>
 * fonttable.c
 * Copyright (C) 2000  Andy Thomas <alt@gimp.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_RENDER_H

#include "freetype-intl.h"
#include "freetype-types.h"

#include "fontsel.h"
#include "fonttable.h"
#include "main.h"
#include "render.h"


#define TRUNC(x)  ((x) >> 6)
#define NUM_PER_PAGE 256
#define POPUP_MAG 4

typedef struct
{
  GtkWidget    *preview;
  GtkWidget    *label;
  GtkWidget    *popup;
  FT_Face       face;
  guchar       *preview_buffer;
  gint          num_glyphs;
  gint          dragging;
  gint          cell_width;
  gint          cell_height;
  gint          pageno;
} PreviewData;

static  double  size = 24;

void fill_preview_buffer(PreviewData  *preview_data,
			 gint          pageno,
			 gint          width,
			 gint          height)
{
  FT_Error      error;
  FT_Bitmap    *source;
  FT_Pos        x_top = 0;
  FT_Pos        y_top = 0;
  FT_GlyphSlot  slot;
  FT_Vector     origin;
  FT_Face       face;

  gint    preview_x = 0;
  gint    preview_y = 0;
  gint    loop_column;
  gint    loop_row;
  gint    maxnum = NUM_PER_PAGE;
  guchar *preview_buffer;
  gint    cell_width;
  gint    cell_height;

  face = preview_data->face;
  preview_buffer = preview_data->preview_buffer;
  cell_width = preview_data->cell_width;
  cell_height = preview_data->cell_height;

  origin.x = 0;
  origin.y = 0;
  y_top = cell_height;

  /*
   * Put glyphs; i=column, j=row
   */

  for (loop_row = 0; loop_row <= (maxnum - 1) / 16; loop_row++)
    for (loop_column = 0; loop_column <= (maxnum - 1) / 16; loop_column++)
      {
	FT_UShort idx = NUM_PER_PAGE * pageno + 16 * loop_row + loop_column;

	/* load the glyph image */
	if (idx >= face->num_glyphs)
	  return;

	x_top = cell_width * loop_column;
	y_top = cell_height * (loop_row + 1);

	error = FT_Load_Glyph (face, idx,
			       FT_LOAD_DEFAULT);

	if (error)
	  {
	    g_message ("FT_Load_Glyph returned %x", (gint) error);
	    return;
	  }

	error = FT_Render_Glyph (face->glyph, ft_render_mode_normal);

	if (!error)
	  {
	    slot = face->glyph;
	    source = &slot->bitmap;

	    /* now render the bitmap into the display surface */

	    blit_glyph_to_buffer (preview_buffer,
				  width, height,
				  preview_x, preview_y,
				  source,
				  x_top + slot->bitmap_left + (cell_width-TRUNC (slot->metrics.width)) / 2,
				  y_top - slot->bitmap_top - (cell_height-TRUNC (slot->metrics.height)) / 2,
				  TRUE);
	  }
      }
}

static void
fill_popup_preview (PreviewData  *preview_data,
		    gint          gnum,
		    gint         *pwidth,
		    gint         *pheight)
{
  GtkWidget    *preview;
  FT_UShort     idx = gnum;
  FT_Face       face;
  FT_Vector     origin;
  FT_GlyphSlot  slot;
  FT_Bitmap    *source;
  FT_Error      error;

  guchar       *buffer;
  gint          i;
  gint          width;
  gint          height;

  face = preview_data->face;

  /* load the glyph image */
  if (idx >= face->num_glyphs)
    return;

  origin.x = 0;
  origin.y = 0;

  error = FT_Set_Char_Size (face,
			    (POPUP_MAG * ((gint) size)) << 6,
			    (POPUP_MAG * ((gint) size)) << 6,
			    (gint) 72,
			    (gint) 72);
  if (error)
    {
      g_message ("FT_Set_Char_Size returned %x", (gint) error);
      return;
    }

  error = FT_Load_Glyph (face, idx,
			 FT_LOAD_DEFAULT);


  if (error)
    {
      g_message ("FT_Load_Glyph returned %x", (gint) error);
      return;
    }

  error = FT_Render_Glyph (face->glyph, ft_render_mode_normal);

  if (!error)
    {
      slot = face->glyph;
      source = &slot->bitmap;
      *pwidth  = width  = source->width;
      *pheight = height = source->rows;

      preview =
        (GtkWidget *) g_object_get_data (G_OBJECT (preview_data->popup),
                                         "preview_widget");

      gtk_preview_size (GTK_PREVIEW (preview), width, height);

      /* now render the bitmap into the display surface */
      buffer = g_new (guchar, width * height);
      memset (buffer, 0xff, width * height);

      blit_glyph_to_buffer (buffer,
			    width, height,
			    0, 0,
			    source,
			    0,
			    0,
			    TRUE);

      for (i = 0; i < height; i++)
	{
	  gtk_preview_draw_row (GTK_PREVIEW (preview),
				buffer + width * i,
				0, i, width);
	}

      g_free (buffer);

      gtk_widget_queue_draw (preview);
    }
}

static gint
preview_events (GtkWidget *widget,
		GdkEvent  *event,
		gpointer   data)
{
  PreviewData    *preview = data;
  gint            x, y;
  GdkEventButton *bevent;
  gchar          *str = NULL;
  gint            idx;
  gint            width,height;

  gtk_widget_get_pointer (widget, &x, &y);

  idx = ((x / preview->cell_width) +
	 (y / preview->cell_height) * 16 +
	 NUM_PER_PAGE * preview->pageno);

  switch (event->type)
    {
    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;
      switch (bevent->button)
	{
	case 3:
	  if(idx > preview->num_glyphs-1)
	    return FALSE;

 	  preview->dragging = TRUE;
	  fill_popup_preview (preview, idx, &width, &height);
 	  gtk_grab_add (widget);
          gtk_window_set_position (GTK_WINDOW (preview->popup),
                                   GTK_WIN_POS_MOUSE);
	  gtk_widget_show (preview->popup);
	  break;

	default:
	  return FALSE;
	}
      break;

    case GDK_BUTTON_RELEASE:
      gtk_widget_hide (preview->popup);
      gtk_grab_remove (widget);
      break;

    case GDK_MOTION_NOTIFY:
      str = g_strdup_printf ("%d",
			     (x / preview->cell_width) +
			     (y / preview->cell_height) * 16 +
			     NUM_PER_PAGE * preview->pageno);

      gtk_label_set_text (GTK_LABEL (preview->label), str);
      g_free (str);
      break;

    default:
      break;
    }

  return FALSE;
}

static GtkWidget *
create_popup_view (gint width,
		   gint height)
{
  GtkWidget *large_popup;
  GtkWidget *frame;
  GtkWidget *vbox;
  GtkWidget *preview;

  large_popup = gtk_window_new (GTK_WINDOW_POPUP);
  gtk_window_set_policy (GTK_WINDOW (large_popup),
			 FALSE, FALSE, TRUE);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
  gtk_container_add (GTK_CONTAINER (large_popup), frame);
  gtk_container_set_border_width (GTK_CONTAINER (large_popup),5);
  gtk_widget_show (frame);

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (frame), vbox);
  gtk_widget_show (vbox);

  preview = gtk_preview_new (GTK_PREVIEW_GRAYSCALE);
  gtk_box_pack_start (GTK_BOX(vbox), preview, TRUE, TRUE, 0);
  gtk_widget_show (preview);

  gtk_preview_size (GTK_PREVIEW (preview), width, height);

  g_object_set_data (G_OBJECT (large_popup), "preview_widget", preview);

  return large_popup;
}

void
fonttable_dialog_create (void)
{
  GtkWidget   *window;
  GtkWidget   *gvbox;
  GtkWidget   *vbox;
  GtkWidget   *notebook;
  GtkWidget   *scwindow;
  GtkWidget   *preview;
  GtkWidget   *label;
  GtkWidget   *info_label;
  GtkWidget   *evb;
  GtkWidget   *large_popup;

  FontFace    *font_face;

  FT_BBox      bbox;
  FT_Face      face;
  FT_Error     error;

  gint         sizex, sizey;
  gint         width, height;
  gint         width_w, height_w;
  guchar      *buffer;
  guchar      *black_buffer;
  gint         i, j;
  gdouble      xscale, yscale;
  PreviewData *preview_data;
  gint         pageno;

  fontsel_get_selected_entry (&font_face);

  if (font_face == NULL)
    {
      g_message (_("No font selected"));
      return;
    }

  error = FT_New_Face (library,
		       font_face->file_name,
		       font_face->face_index,
		       &face);
  if (error)
    {
      g_message ("FT_New_Face returned %x", error);
      return;
    }

  if (! (face->face_flags & FT_FACE_FLAG_SCALABLE))
    {
      g_message ("Sorry, font is not scalable.");
      return;
    }

  error = FT_Set_Char_Size (face,
			    ((gint) size) << 6,
			    ((gint) size) << 6,
			    (gint) 72,
			    (gint) 72);

  if (error)
    {
      g_message ("FT_Set_Char_Size returned %x", (int) error);
      return;
    }

  /* Setup local data for this widget */

  bbox.xMin = face->bbox.xMin;
  bbox.yMin = face->bbox.yMin;
  bbox.xMax = face->bbox.xMax;
  bbox.yMax = face->bbox.yMax;

  sizex = bbox.xMax - bbox.xMin;  /* In font units */
  sizey = bbox.yMax - bbox.yMin;

#if 0
  /* Hmm I think freetype has a bug... */
  g_print (" bbox.yMax %d bbox.yMin %d\n",
	   (int)face->bbox.yMax, (int)face->bbox.yMin);
#endif /* 0 */

  if (sizex <= 0 || sizey <= 0)
    {
      g_message ("Invalid bounding box in font");
      return;
    }

  /* Needs to take account of gimp units etc */
  /*   xscale = xresolution / 72.0; */
  /*   yscale = yresolution / 72.0; */

  xscale = yscale = 72.0 / 72.0;

  /*
   * Get the size of the cell surrounding the glyph: it should be 1.2 times the
   * size of the glyph
   */

  /* Pixels */
  width  = (gint) ((sizex * size * xscale * 1.2) / face->units_per_EM);
  height = (gint) ((sizey * size * yscale * 1.4) / face->units_per_EM);

#ifdef FT_DEBUG
  /* More evidence of the bug */
  g_print ("height = %d\n", (int)height);
  g_print ("width = %d\n", (int)width);
#endif
  width_w = width * 16 + 1;   /* 16 cells in a row or column */
  height_w = height * 16 + 1;

  /*
   * Create window and image
   */

  window = gimp_dialog_new (font_face->family_name, "font-table",
                            NULL, 0, NULL, NULL,

                            GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,

			    NULL);

  g_signal_connect (window, "response",
                    G_CALLBACK (gtk_widget_destroy),
                    NULL);
  g_signal_connect_swapped (window, "destroy",
                            G_CALLBACK (FT_Done_Face),
                            (gpointer) face);

  /*
   * Global vertical container
   */

  gvbox = gtk_vbox_new (FALSE, 6);
  gtk_container_set_border_width (GTK_CONTAINER (gvbox), 12);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (window)->vbox), gvbox);
  gtk_widget_show (gvbox);

  /*
   * Vertical container
   */

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX(gvbox), vbox, TRUE, TRUE, 0);
  gtk_widget_show (vbox);

  /*
   * Notebook
   */

  notebook = gtk_notebook_new ();
  gtk_notebook_set_show_border (GTK_NOTEBOOK(notebook), TRUE);
  gtk_box_pack_start (GTK_BOX (vbox), notebook, TRUE, TRUE, 0);
  gtk_widget_show (notebook);

  /*
   * Info label
   */

  evb = gtk_event_box_new ();
  gtk_widget_set_name (evb, "info label");
  gtk_box_pack_end (GTK_BOX (gvbox), evb, FALSE, FALSE, 0);
  info_label = gtk_label_new (NULL);
  gtk_misc_set_padding (GTK_MISC (info_label), 0, 2);
  gtk_container_add (GTK_CONTAINER (evb), info_label);
  gtk_widget_show (info_label);
  gtk_widget_show (evb);

  /*
   * Popup window for larger display of widget.
   */

  large_popup = create_popup_view (POPUP_MAG * width, POPUP_MAG * height);

  /*
   * Now add a preview buffer
   * Add pages one for each grouping..
   */

  for (pageno = 0 ; pageno <= (face->num_glyphs) / NUM_PER_PAGE; pageno++)
    {
      guchar *str;
      gint    lastidx = (pageno + 1) * NUM_PER_PAGE - 1;

      scwindow = gtk_scrolled_window_new (NULL, NULL);
      gtk_widget_show (scwindow);
      gtk_container_set_border_width (GTK_CONTAINER (scwindow), 2);
      gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scwindow),
				      GTK_POLICY_ALWAYS, GTK_POLICY_ALWAYS);

      if (lastidx > face->num_glyphs)
	lastidx = face->num_glyphs - 1;

      str = g_strdup_printf (_("Glyphs %d-%d"),
			     pageno * NUM_PER_PAGE,
			     lastidx);
      label = gtk_label_new(str);
      gtk_notebook_append_page (GTK_NOTEBOOK (notebook), scwindow, label);
      gtk_widget_show (label);
      g_free (str);

      preview = gtk_preview_new (GTK_PREVIEW_GRAYSCALE);
      gtk_widget_show (preview);
      gtk_widget_set_events (preview,
			     GDK_BUTTON_PRESS_MASK |
			     GDK_BUTTON_RELEASE_MASK |
			     GDK_BUTTON_MOTION_MASK |
			     GDK_POINTER_MOTION_MASK |
			     GDK_POINTER_MOTION_HINT_MASK);
      gtk_preview_size (GTK_PREVIEW (preview), width_w, height_w);
      gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scwindow),
					     preview);
      /*
       * Clear it out
       */

      buffer = g_new (guchar, width_w * height_w);
      memset (buffer, 0xff, width_w * height_w);

      black_buffer = g_new0 (guchar, width_w);

      preview_data = g_new0 (PreviewData, 1);
      preview_data->face           = face;
      preview_data->label          = info_label;
      preview_data->preview_buffer = buffer;
      preview_data->preview        = preview;
      preview_data->cell_width     = width;
      preview_data->cell_height    = height;
      preview_data->pageno         = pageno;
      preview_data->popup          = large_popup;
      preview_data->num_glyphs     = face->num_glyphs;

      g_signal_connect (preview, "event",
                        G_CALLBACK (preview_events),
                        preview_data);

      fill_preview_buffer (preview_data, pageno, width_w, height_w);

      for (i = 0; i < height_w; i++)
	{
	  /* put in cell left boundaries & last right one*/
	  for (j = 0 ; j < width_w; j++)
	    {
	      if (j % width == 0)
		*(j+buffer + width_w * i) = 0;
	    }

	  if (i % height == 0)
	    /* Draw top line of cell*/
	    gtk_preview_draw_row (GTK_PREVIEW (preview),
				  black_buffer,
				  0, i, width_w);
	  else
	    gtk_preview_draw_row (GTK_PREVIEW (preview),
				  buffer + width_w * i,
				  0, i, width_w);
	}

      g_free (buffer);
      g_free (black_buffer);

      gtk_widget_queue_draw (preview);
    }

  /* Set the size so that the complete fontmap is visible on screen,
   * but only up to the screen size.
   */

  gtk_widget_set_usize (window,
			MIN (width_w,  gdk_screen_width () - 30),
			MIN (height_w, gdk_screen_height () - 40));
  gtk_widget_show (window);
}
