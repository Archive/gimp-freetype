/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GIMP FreeType Plug-in
 * Copyright (C) 2000-2003  Sven Neumann <sven@gimp.org>
 *                          Jens Lautenbacher <jtl@gimp.org>
 *
 * This plug-in contains code taken from the freetype2 library and
 * its demos, which is licensed under The FreeType Project LICENSE.
 *  Copyright 1996-2000 by
 *   David Turner, Robert Wilhelm, and Werner Lemberg
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <string.h>

#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>

#include <ft2build.h>
#include FT_MODULE_H

#include "freetype-intl.h"
#include "freetype-types.h"

#include "main.h"
#include "interface.h"
#include "render.h"


/* Declare local functions. */

static void        query (void);
static void        run   (const gchar      *name,
			  gint              nparams,
			  const GimpParam  *param,
			  gint             *nreturn_vals,
			  GimpParam       **return_vals);

GimpPlugInInfo PLUG_IN_INFO =
{
  NULL,  /* init_proc  */
  NULL,  /* quit_proc  */
  query, /* query_proc */
  run,   /* run_proc   */
};

static FreeTypeVals vals =
{
  "/path/to/some/font.ttf",
  18,
  (gint32) GIMP_UNIT_POINT,
  { 1.0, 0.0, 0.0, 1.0 },
  TRUE,
  TRUE,
  TRUE,
  FALSE,
  0,
  "The quick brown fox jumps over the lazy dog"
};

static FreeTypeRetVals retvals =
{
  GIMP_PDB_SUCCESS,
  -1
};

FreeTypeUIVals ui =
{
  "Foo",
  "Bar",
  { 1.0, 0.0, 0.0, 1.0 },
  0.0
};


/*  variables declared extern  */
FT_Library    library;


MAIN ()


static
void query (void)
{
  gchar *help_path;
  gchar *help_uri;

  static GimpParamDef args[] =
  {
    { GIMP_PDB_INT32,    "run_mode",     "Interactive, non-interactive"  },
    { GIMP_PDB_IMAGE,    "image",        "Input image"                   },
    { GIMP_PDB_DRAWABLE, "drawable",     "Input drawable"                },
    { GIMP_PDB_STRING,   "font_file",    "Font file"                     },
    { GIMP_PDB_FLOAT,    "size",         "Size"                          },
    { GIMP_PDB_INT32,    "unit_id",      "The integer id of size's unit" },
    { GIMP_PDB_FLOAT,    "transform_xx", "Transformation Matrix"         },
    { GIMP_PDB_FLOAT,    "transform_xy", "Transformation Matrix"         },
    { GIMP_PDB_FLOAT,    "transform_yx", "Transformation Matrix"         },
    { GIMP_PDB_FLOAT,    "transform_yy", "Transformation Matrix"         },
    { GIMP_PDB_INT32,    "kerning",      "Kerning on/off"                },
    { GIMP_PDB_INT32,    "hinting",      "Hinting on/off"                },
    { GIMP_PDB_INT32,    "antialiasing", "Antialiasing on/off"           },
    { GIMP_PDB_INT32,    "outline",      "Bezier outline on/off"         },
    { GIMP_PDB_INT32,    "spacing",      "Letter spacing"                },
    { GIMP_PDB_STRING,   "text",         "Text"                          }
  };
  static GimpParamDef return_vals[] =
  {
    { GIMP_PDB_LAYER,    "layer",        "The text layer"                }
  };

  gimp_plugin_domain_register (GETTEXT_PACKAGE, LOCALEDIR);

  help_path = g_build_filename (DATADIR, "help", NULL);
  help_uri = g_filename_to_uri (help_path, NULL, NULL);
  g_free (help_path);

  gimp_plugin_help_register ("http://freetype.gimp.org/help", help_uri);

  g_free (help_uri);

  gimp_install_procedure ("plug_in_freetype",
			  "FreeType Renderer",
			  "No help yet",
			  "Sven Neumann, Jens Lautenbacher, Michael Natterer",
			  "Sven Neumann, Jens Lautenbacher, Michael Natterer",
			  "2000-2003",
			  /* Don't translate the text in brackets (<Image>) */
			  N_("<Image>/Filters/Text/FreeType..."),
			  "RGB*, GRAY*",
			  GIMP_PLUGIN,
			  G_N_ELEMENTS (args), G_N_ELEMENTS (return_vals),
			  args, return_vals);
}

static void
run (const gchar      *name,
     gint              n_params,
     const GimpParam  *param,
     gint             *nreturn_vals,
     GimpParam       **return_vals)
{
  static GimpParam   values[2];
  GimpDrawable      *drawable;
  gint32             image_ID;
  GimpRunMode        run_mode;
  GimpPDBStatusType  status = GIMP_PDB_SUCCESS;
  FT_Error           error;
  FontFace          *font_face = NULL;

  *nreturn_vals = 2;
  *return_vals  = values;

  run_mode = param[0].data.d_int32;
  image_ID = param[1].data.d_int32;

  values[0].type          = GIMP_PDB_STATUS;
  values[0].data.d_status = GIMP_PDB_SUCCESS;
  values[1].type          = GIMP_PDB_LAYER;
  values[1].data.d_int32  = -1;

  drawable = gimp_drawable_get (param[2].data.d_drawable);

  /*  Initialize i18n support  */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
#ifdef HAVE_BIND_TEXTDOMAIN_CODESET
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
#endif
  textdomain (GETTEXT_PACKAGE);

  /* Initialize engine */
  error = FT_Init_FreeType (&library);
  if (error)
    {
      g_message ("Could not initialize FreeType library");
      status = GIMP_PDB_EXECUTION_ERROR;
    }
  init_glyphs ();

  if (status == GIMP_PDB_SUCCESS)
    {
      switch (run_mode)
	{
	case GIMP_RUN_NONINTERACTIVE:
	  if (n_params != 16)
	    status = GIMP_PDB_CALLING_ERROR;

	  if (status == GIMP_PDB_SUCCESS)
	    {
	      strncpy (vals.font_file,   param[3].data.d_string, BUF_SIZE);
	      vals.size                = param[4].data.d_float;
	      vals.unit     = (GimpUnit) param[5].data.d_int32;
	      vals.transform.xx        = param[6].data.d_float;
	      vals.transform.xy        = param[7].data.d_float;
	      vals.transform.yx        = param[8].data.d_float;
	      vals.transform.yy        = param[9].data.d_float;
	      vals.kerning             = param[10].data.d_int32;
	      vals.hinted              = param[11].data.d_int32;
	      vals.antialias           = param[12].data.d_int32;
	      vals.outline             = param[13].data.d_int32;
	      vals.spacing             = param[14].data.d_int32;
	      strncpy (vals.text,        param[15].data.d_string, BUF_SIZE);

	      font_face = g_new0 (FontFace, 1);
	      font_face->file_name = vals.font_file;
	    }
	  break;

	case GIMP_RUN_INTERACTIVE:
	  /*  Possibly retrieve data  */
	  gimp_procedural_db_get_data ("plug_in_freetype", &vals);
	  gimp_procedural_db_get_data ("plug_in_freetype_ui", &ui);

	  font_face = dialog (image_ID, &vals, &ui);

	  if (!font_face)
	    status = GIMP_PDB_EXECUTION_ERROR;
	  break;

	case GIMP_RUN_WITH_LAST_VALS:
	  /* Possibly retrieve data */
	  gimp_procedural_db_get_data ("plug_in_freetype", &vals);
	  font_face = g_new0 (FontFace, 1);
	  font_face->file_name = vals.font_file;
	  break;

	default:
	  break;
	}
    }

  if (status == GIMP_PDB_SUCCESS)
    {
      if (render_prepare (image_ID,
			  FALSE, NULL, &vals, font_face, UPDATE_FULL))
	render (image_ID, NULL, 0, 0, 0, 0, &vals, &retvals, font_face);

      if (run_mode != GIMP_RUN_NONINTERACTIVE)
	gimp_displays_flush ();

      if (run_mode == GIMP_RUN_INTERACTIVE)
	{
	  strncpy (vals.font_file, font_face->file_name,   BUF_SIZE);
	  strncpy (ui.font_family, font_face->family_name, BUF_SIZE);
	  strncpy (ui.font_style,  font_face->style_name,  BUF_SIZE);

	  gimp_procedural_db_set_data ("plug_in_freetype",
				       &vals, sizeof (vals));
	  gimp_procedural_db_set_data ("plug_in_freetype_ui",
				       &ui, sizeof (ui));
	}

      gimp_drawable_detach (drawable);
    }

  if (!error)
    {
      free_glyphs ();
      FT_Done_FreeType (library);
    }

   values[0].data.d_status = status;
   values[1].data.d_int32  = retvals.layerid;
}
