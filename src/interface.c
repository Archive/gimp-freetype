/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GIMP FreeType Plug-in
 * Copyright (C) 2000-2003  Sven Neumann <sven@gimp.org>
 *                          Jens Lautenbacher <jtl@gimp.org>
 *
 * This plug-in contains code taken from the freetype2 library and
 * its demos, which is licensed under The FreeType Project LICENSE.
 *  Copyright 1996-2000 by
 *   David Turner, Robert Wilhelm, and Werner Lemberg
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <string.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>

#include "freetype-intl.h"
#include "freetype-types.h"

#include "about.h"
#include "fontsel.h"
#include "fonttable.h"
#include "interface.h"
#include "main.h"
#include "matrix.h"
#include "render.h"


#define  SPIN_BUTTON_WIDTH   75
#define  SCALE_WIDTH         180

#define  RESPONSE_ABOUT      1
#define  RESPONSE_TABLE      2
#define  RESPONSE_CONFIGURE  3


typedef struct
{
  gint32           image_ID;
  FreeTypeVals    *vals;
  FreeTypeRetVals *retvals;
  RenderUpdate     update;
  GtkWidget       *preview;
  GtkWidget       *entry;
  gint             width;
  gint             height;
  GtkObject       *adj_x;
  GtkObject       *adj_y;
  guint            idle_handler;
  guchar          *buffer;

  /*  cursor handling  */
  FT_Pos          *left;
  FT_Vector        cursor_bottom;
  FT_Vector        cursor_top;

  /*  dragging  */
  gint             mouse_x;
  gint             mouse_y;
  gboolean         dragging;
} PreviewData;


/*  Declare local functions  */

static void        preview_render           (PreviewData   *preview);
static gint        preview_idle_callback    (gpointer       data);
static void        preview_callback         (GtkWidget     *widget,
					     gpointer       data);
static void        auto_preview_callback    (GtkWidget     *widget,
					     gpointer       data);
static void        preview_size_changed     (GtkWidget     *widget,
					     GtkAllocation *allocation,
					     gpointer       data);
static gint        preview_events           (GtkWidget     *widget,
					     GdkEvent      *event,
					     gpointer       data);
static void        preview_cursor_draw      (PreviewData   *preview,
					     GdkRectangle  *area);
static void        preview_cursor_set       (PreviewData   *preview,
					     gint           position);
static gint        preview_cursor_find_pos  (PreviewData   *preview,
					     gint           x,
					     gint           y);
static void        response_callback        (GtkWidget     *dialog,
                                             gint           response_id,
					     gpointer       data);
static void        update_zoom_factor_label (void);
static void        zoom_in_callback         (GtkWidget     *widget,
					     gpointer       data);
static void        zoom_out_callback        (GtkWidget     *widget,
					     gpointer       data);
static void        dot_for_dot_callback     (GtkWidget     *widget,
					     gpointer       data);
static void        unit_callback            (GtkWidget     *widget,
					     gpointer       data);
static void        transform_callback       (GtkWidget     *adjustment,
					     gpointer       data);
static void        transform_reset_callback (GtkWidget     *widget,
					     gpointer       data);
static void        text_callback            (GtkWidget     *widget,
					     gpointer       data);
static gint        progress_timeout         (gpointer       data);
static GtkWidget * create_general_page      (PreviewData   *preview,
                                             ProgressData  *pdata);


/*  Local variables  */

static FreeTypeUIVals *uivals              = NULL;

static gboolean        dot_for_dot         = TRUE;
static gint            zoom_factor_src     = 1;
static gint            zoom_factor_dest    = 1;

static gboolean        auto_update_preview = TRUE;

static gdouble         image_xres          = 0.0;
static gdouble         image_yres          = 0.0;
static gdouble         monitor_xres        = 0.0;
static gdouble         monitor_yres        = 0.0;

static GtkObject      *rotation_adj        = NULL;
static GtkObject      *matrix_xx_adj       = NULL;
static GtkObject      *matrix_xy_adj       = NULL;
static GtkObject      *matrix_yy_adj       = NULL;

static GtkWidget      *zoom_factor_label   = NULL;

static GtkWidget      *paned               = NULL;


/*  Local functions  */

static void
preview_render (PreviewData *preview)
{
  FontFace      *font_face;
  GtkAdjustment *adj_x;
  GtkAdjustment *adj_y;
  Matrix2        saved_transform = preview->vals->transform;
  Matrix2        scale_transform = { 1.0, 0.0, 0.0, 1.0 };
  const FT_BBox *bbox;

  if (! GTK_WIDGET_DRAWABLE (GTK_WIDGET (preview->preview)))
    return;

  fontsel_get_selected_entry (&font_face);
  if (!font_face)
    return;

  preview->width  = preview->preview->allocation.width;
  preview->height = preview->preview->allocation.height;

  if (!preview->width || !preview->height)
    return;

  adj_x = GTK_ADJUSTMENT (preview->adj_x);
  adj_y = GTK_ADJUSTMENT (preview->adj_y);

  if (monitor_xres == 0.0)
    gimp_get_monitor_resolution (&monitor_xres, &monitor_yres);

  if (!dot_for_dot)
    {
      gdouble zoom_factor;

      zoom_factor = (gdouble) zoom_factor_src / (gdouble) zoom_factor_dest;

      scale_transform.xx = (monitor_xres / image_xres * zoom_factor);
      scale_transform.yy = (monitor_yres / image_yres * zoom_factor);

      preview->vals->transform = matrix2_mult (scale_transform,
					       saved_transform);
    }

  preview->left =
    g_realloc (preview->left,
	       (strlen (preview->vals->text) + 1) * sizeof (FT_Pos));

  bbox = render_prepare (preview->image_ID,
			 TRUE,
			 preview->left,
			 preview->vals,
			 font_face,
			 preview->update);

  if (bbox == NULL)
    return;

  preview->vals->transform = saved_transform;

  adj_x->upper     = bbox->xMax - bbox->xMin;
  adj_x->page_size = preview->width;
  gtk_adjustment_changed (adj_x);
  if (adj_x->value + adj_x->page_size > adj_x->upper)
    gtk_adjustment_set_value (adj_x, adj_x->upper - adj_x->page_size);

  adj_y->upper     = bbox->yMax - bbox->yMin;
  adj_y->page_size = preview->height;
  gtk_adjustment_changed (adj_y);
  if (adj_y->value + adj_y->page_size > adj_y->upper)
    gtk_adjustment_set_value (adj_y, adj_y->upper - adj_y->page_size);

  if (preview->buffer)
    preview->buffer = g_realloc (preview->buffer,
				 preview->width * preview->height);
  else
    preview->buffer = g_new (guchar, preview->width * preview->height);

  memset (preview->buffer, 255, preview->width * preview->height);

  render (preview->image_ID,
	  preview->buffer,
	  preview->width,
	  preview->height,
	  adj_x->value,
	  adj_y->value,
	  preview->vals,
	  preview->retvals,
	  font_face);

  gtk_widget_queue_draw (preview->preview);

  preview->update = 0;
}

static gint
preview_idle_callback (gpointer data)
{
  PreviewData *preview = (PreviewData *)data;

  preview_render (preview);
  preview_cursor_set (preview,
		      gtk_editable_get_position (GTK_EDITABLE (preview->entry)));
  preview_cursor_draw (preview, NULL);
  preview->idle_handler = 0;

  return FALSE;
}

static void
preview_callback (GtkWidget *widget,
		  gpointer   data)
{
  PreviewData *preview = (PreviewData *)data;

  if (preview->idle_handler == 0)
    preview->idle_handler = g_idle_add ((GSourceFunc) preview_idle_callback,
                                        preview);
}

static void
auto_preview_callback (GtkWidget *widget,
		       gpointer   data)
{
  PreviewData  *preview = (PreviewData *)data;
  RenderUpdate  update;

  update = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (widget), "update"));
  preview->update |= update;

  if (auto_update_preview)
    preview_callback (NULL, preview);
}

static void
preview_size_changed (GtkWidget     *widget,
		      GtkAllocation *allocation,
 		      gpointer       data)

{
  preview_callback (widget, data);
}

static gint
preview_cursor_find_pos (PreviewData *preview,
			 gint         x,
			 gint         y)
{
  FT_Vector left;
  gint offset_x;
  gint i;

  offset_x = GTK_ADJUSTMENT (preview->adj_x)->value;

  for (i = 0; i < strlen (preview->vals->text); i++)
    {
      left.x = preview->left[i+1];
      left.y = 0;

      matrix2_transform_point (preview->vals->transform, &left);

      if ((left.x >> 6) > x + offset_x)
	break;
    }

  return i;
}

static void
preview_cursor_set (PreviewData *preview,
		    gint         position)
{
  gint height = GTK_ADJUSTMENT (preview->adj_y)->upper;

  preview->cursor_top.x    = preview->left[position];
  preview->cursor_top.y    = 0;
  preview->cursor_bottom.x = preview->left[position];
  preview->cursor_bottom.y = height << 6;

}

static void
preview_cursor_draw (PreviewData  *preview,
		     GdkRectangle *area)
{
#ifdef DRAW_BROKEN_CURSOR
  static GdkGC *cursor_gc = NULL;
  gint          offset_x;
  gint          offset_y;
  FT_Vector     top;
  FT_Vector     bottom;

  if (cursor_gc == NULL)
    {
      GdkColor white;

      gdk_color_white (gtk_widget_get_colormap (preview->preview), &white);

      cursor_gc = gdk_gc_new (preview->preview->window);
      gdk_gc_set_foreground (cursor_gc, &white);
      gdk_gc_set_line_attributes (cursor_gc, 1,
				  GDK_LINE_SOLID, GDK_CAP_BUTT, GDK_JOIN_MITER);
      gdk_gc_set_function (cursor_gc, GDK_XOR);
    }

  offset_x = GTK_ADJUSTMENT (preview->adj_x)->value;
  offset_y = GTK_ADJUSTMENT (preview->adj_y)->value;

  top    = preview->cursor_top;
  bottom = preview->cursor_bottom;

  matrix2_transform_point (preview->vals->transform, &top);
  matrix2_transform_point (preview->vals->transform, &bottom);

  gdk_gc_set_clip_rectangle (cursor_gc, area);
  gdk_draw_line (preview->preview->window, cursor_gc,
		 (top.x    >> 6) - offset_x, (top.y    >> 6) - offset_y,
		 (bottom.x >> 6) - offset_x, (bottom.y >> 6) - offset_y);
#endif  /* DRAW_BROKEN_CURSOR  */
}

static gint
preview_events (GtkWidget *widget,
		GdkEvent  *event,
		gpointer   data)
{
  PreviewData    *preview = (PreviewData *)data;
  GdkEventButton *bevent;
  GtkAdjustment  *adj;
  gint            x, y;
  gint            dx, dy;

  gtk_widget_get_pointer (widget, &x, &y);

  switch (event->type)
    {
    case GDK_BUTTON_PRESS:
      bevent = (GdkEventButton *) event;
      switch (bevent->button)
	{
	case 1:
	  preview_cursor_draw (preview, NULL);
	  gtk_editable_set_position (GTK_EDITABLE (preview->entry),
				     preview_cursor_find_pos (preview, x, y));
	  gtk_widget_grab_focus (preview->entry);
	  preview_cursor_set (preview,
			      gtk_editable_get_position (GTK_EDITABLE (preview->entry)));
	  preview_cursor_draw (preview, NULL);
	  break;

	case 2:
	  preview->mouse_x  = x;
	  preview->mouse_y  = y;
	  preview->dragging = TRUE;
	  gtk_grab_add (widget);
	  break;

	case 4:
	  if (bevent->state & GDK_SHIFT_MASK && !dot_for_dot)
	    {
	      zoom_in_callback (NULL, NULL);
	      preview->update |= UPDATE_TRANSFORM;
	      preview_callback (NULL, preview);
	    }
	  else
	    {
	      adj = ((bevent->state & GDK_CONTROL_MASK) ?
                     GTK_ADJUSTMENT (preview->adj_x)    :
                     GTK_ADJUSTMENT (preview->adj_y));
	      x = CLAMP (adj->value - adj->page_increment,
			 adj->lower, adj->upper - adj->page_size);
	      gtk_adjustment_set_value (adj, x);
	    }
	  break;

	case 5:
	  if (bevent->state & GDK_SHIFT_MASK && !dot_for_dot)
	    {
	      zoom_out_callback (NULL, NULL);
	      preview->update |= UPDATE_TRANSFORM;
	      preview_callback (NULL, preview);
	    }
	  else
	    {
	      adj = ((bevent->state & GDK_CONTROL_MASK) ?
                     GTK_ADJUSTMENT (preview->adj_x)    :
                     GTK_ADJUSTMENT (preview->adj_y));
	      x = CLAMP (adj->value + adj->page_increment,
			 adj->lower, adj->upper - adj->page_size);
	      gtk_adjustment_set_value (adj, x);
	    }
	  break;

	default:
	  return FALSE;
	}
      break;

    case GDK_BUTTON_RELEASE:
      if (preview->dragging)
	{
	  gtk_grab_remove (widget);
	  preview->dragging = FALSE;
	}
      break;

    case GDK_MOTION_NOTIFY:
      if (preview->dragging)
	{
	  dx = x - preview->mouse_x;
	  dy = y - preview->mouse_y;

	  preview->mouse_x = x;
	  preview->mouse_y = y;

	  if (dx)
	    {
	      adj = GTK_ADJUSTMENT (preview->adj_x);
	      x = CLAMP (adj->value - dx,
			 adj->lower, adj->upper - adj->page_size);
	      gtk_adjustment_set_value (adj, x);
	    }
	  if (dy)
	    {
	      adj = GTK_ADJUSTMENT (preview->adj_y);
	      y = CLAMP (adj->value - dy,
			 adj->lower, adj->upper - adj->page_size);
	      gtk_adjustment_set_value (adj, y);
	    }
	}
      break;

    case GDK_EXPOSE:
      {
	GdkEventExpose *eevent = (GdkEventExpose *)event;

	if (preview->buffer == NULL)
	  return FALSE;

	gdk_gc_set_clip_rectangle (preview->preview->style->black_gc,
				   &eevent->area);
	gdk_draw_gray_image (preview->preview->window,
			     preview->preview->style->black_gc,
			     0, 0, preview->width, preview->height,
			     GDK_RGB_DITHER_NORMAL,
			     preview->buffer,
			     preview->width);
	preview_cursor_draw (preview, &eevent->area);
	gdk_gc_set_clip_rectangle (preview->preview->style->black_gc, NULL);
      }
      break;

    default:
      break;
    }

  return FALSE;
}

static void
response_callback (GtkWidget *dialog,
                   gint       response_id,
                   gpointer   data)
{
  switch (response_id)
    {
    case RESPONSE_ABOUT:
      about_dialog_run (dialog);
      break;

    case RESPONSE_TABLE:
      fonttable_dialog_create ();
      break;

    case RESPONSE_CONFIGURE:
      {
        GtkWidget *fontsel;

        gtk_widget_set_sensitive (dialog, FALSE);
        fontsel = g_object_get_data (G_OBJECT (dialog), "fontsel");
        fontsel_configure (GTK_WIDGET (fontsel), FALSE);
        gtk_widget_set_sensitive (dialog, TRUE);
      }
      break;

    case GTK_RESPONSE_OK:
      {
        FontFace **font_face;

        font_face = g_object_get_data (G_OBJECT (dialog), "font_face");
        fontsel_get_selected_entry (font_face);

        if (! *font_face)
          {
            g_message (_("No font selected"));
            break;
          }
      }
      /*  fallthrough  */

    default:
      gtk_widget_destroy (dialog);
      break;
    }
}

static void
update_zoom_factor_label (void)
{
  gchar *str = NULL;

  if (! zoom_factor_label)
    return;

  if (dot_for_dot)
    str = g_strdup ("1:1");
  else
    str = g_strdup_printf ("%d:%d", zoom_factor_src, zoom_factor_dest);

  gtk_label_set_text (GTK_LABEL (zoom_factor_label), str);

  g_free (str);
}

static void
dot_for_dot_callback (GtkWidget *widget,
		      gpointer   data)
{
  gimp_toggle_button_update (widget, data);

  update_zoom_factor_label ();
}

static void
zoom_in_callback (GtkWidget *widget,
		  gpointer   data)
{
  if (zoom_factor_dest > 1)
    zoom_factor_dest--;
  else if (zoom_factor_src < 16)
    zoom_factor_src++;

  update_zoom_factor_label ();
}

static void
zoom_out_callback (GtkWidget *widget,
		   gpointer   data)
{
  if (zoom_factor_src > 1)
    zoom_factor_src--;
  else if (zoom_factor_dest < 16)
    zoom_factor_dest++;

  update_zoom_factor_label ();
}

static void
fontsize_callback (GtkWidget *widget,
		   gpointer   data)
{
  *((gdouble *) data) =
    gimp_size_entry_get_value (GIMP_SIZE_ENTRY (widget), 0);
}

static void
unit_callback (GtkWidget *widget,
	       gpointer   data)
{
  *((GimpUnit *) data) =
    gimp_size_entry_get_unit (GIMP_SIZE_ENTRY (widget));
}

static void
transform_callback (GtkWidget *adjustment,
		    gpointer   data)
{
  FreeTypeVals *vals;
  gdouble       angle;
  gdouble       cosinus;
  gdouble       sinus;
  Matrix2       rot_matrix;

  vals = (FreeTypeVals *) data;

  angle = gimp_deg_to_rad (uivals->rotation);

  cosinus = cos (angle);
  sinus   = sin (angle);

  rot_matrix.xx = cosinus;
  rot_matrix.xy = -sinus;
  rot_matrix.yx = sinus;
  rot_matrix.yy = cosinus;

  vals->transform = matrix2_mult (rot_matrix, uivals->transform);

  if (image_xres != image_yres)
    {
      Matrix2 proj_matrix = { 1.0, 0.0, 0.0, 1.0 };

      if (image_xres > image_xres)
	proj_matrix.xx = image_yres / image_xres;
      else
	proj_matrix.xx = image_xres / image_yres;

      vals->transform = matrix2_mult (proj_matrix, vals->transform);
    }
}

static void
transform_reset_callback (GtkWidget *adjustment,
			  gpointer   data)
{
  gtk_adjustment_set_value (GTK_ADJUSTMENT (matrix_xx_adj), 1.0);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (matrix_xy_adj), 0.0);
  gtk_adjustment_set_value (GTK_ADJUSTMENT (matrix_yy_adj), 1.0);

  gtk_adjustment_set_value (GTK_ADJUSTMENT (rotation_adj), 0.0);
}

static void
text_callback (GtkWidget *widget,
	       gpointer   data)
{
  gchar       *string = (gchar *)data;
  const gchar *tmp;

  tmp = gtk_entry_get_text (GTK_ENTRY (widget));
  if (tmp)
    strncpy (string, tmp, BUF_SIZE);
}

static void
notebook_callback (GtkNotebook     *notebook,
		   GtkNotebookPage *page,
		   gint             page_num,
		   gpointer         data)
{
  GtkWidget *fontsel = GTK_WIDGET (data);

  if (!paned)
    return;

  switch (page_num)
    {
    case 0:  /* General */
      if (! GTK_PANED (paned)->child1)
	{
	  gtk_paned_pack1 (GTK_PANED (paned), fontsel, TRUE, FALSE);
	  g_object_unref (G_OBJECT (fontsel));
	}
      break;

    case 1:  /* Transformations */
      if (GTK_PANED (paned)->child1)
	{
	  g_object_ref (G_OBJECT (fontsel));
	  gtk_container_remove (GTK_CONTAINER (paned), fontsel);

	  gtk_paned_set_position (GTK_PANED (paned), 0);
	}
      break;

    default:
      break;
    }
}

static gint
progress_timeout (gpointer data)
{
  GtkAdjustment *adj;
  gfloat         new_val;

  adj = GTK_PROGRESS (data)->adjustment;

  new_val = adj->value + 10;
  if (new_val > adj->upper)
    new_val = adj->lower;

  gtk_progress_set_value (GTK_PROGRESS (data), new_val);

  return TRUE;
}

static GtkWidget *
create_transform_page (PreviewData *preview)
{
  GtkWidget *vbox;
  GtkWidget *table;
  GtkObject *scale;
  GtkWidget *hbox;
  GtkWidget *button;
  gint       row;

  FreeTypeVals *vals = preview->vals;

  vbox = gtk_vbox_new (FALSE, 0);

  table = gtk_table_new (4, 3, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (table), 6);
  gtk_table_set_col_spacings (GTK_TABLE (table), 6);
  gtk_table_set_row_spacings (GTK_TABLE (table), 6);
  gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);

  row = 0;

  scale = rotation_adj =
    gimp_scale_entry_new (GTK_TABLE (table), 0, row++,
			  _("Rotation:"),
			  SCALE_WIDTH, SPIN_BUTTON_WIDTH,
			  uivals->rotation, -180, 180, 1, 15, 1,
			  TRUE, 0.0, 0.0,
			  NULL, NULL);
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (gimp_double_adjustment_update),
                    &uivals->rotation);
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (transform_callback),
                    vals);
  g_object_set_data (G_OBJECT (scale), "update",
                     GINT_TO_POINTER (UPDATE_TRANSFORM));
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  gtk_table_set_row_spacing (GTK_TABLE (table), row, 2);
  scale = matrix_xx_adj =
    gimp_scale_entry_new (GTK_TABLE (table), 0, row++,
			  _("Scale X:"),
			  SCALE_WIDTH, SPIN_BUTTON_WIDTH,
			  uivals->transform.xx, -10, 10,  0.01, 0.1, 2,
			  TRUE, 0.0, 0.0,
			  NULL, NULL);
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (gimp_double_adjustment_update),
                    &uivals->transform.xx);
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (transform_callback),
                    vals);
  g_object_set_data (G_OBJECT (scale), "update",
                     GINT_TO_POINTER (UPDATE_TRANSFORM));
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  scale = matrix_yy_adj =
    gimp_scale_entry_new (GTK_TABLE (table), 0, row++,
			  _("Scale Y:"),
			  SCALE_WIDTH, SPIN_BUTTON_WIDTH,
			  uivals->transform.yy, -10, 10,  0.01, 0.1, 2,
			  TRUE, 0.0, 0.0,
			  NULL, NULL);
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (gimp_double_adjustment_update),
                    &uivals->transform.yy);
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (transform_callback),
                    vals);
  g_object_set_data (G_OBJECT (scale), "update",
                     GINT_TO_POINTER (UPDATE_TRANSFORM));
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  scale = matrix_xy_adj =
    gimp_scale_entry_new (GTK_TABLE (table), 0, row++,
			  _("Shear:"),
			  SCALE_WIDTH, SPIN_BUTTON_WIDTH,
			  uivals->transform.xy, -10, 10,  0.01, 0.1, 2,
			  TRUE, 0.0, 0.0,
			  NULL, NULL);
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (gimp_double_adjustment_update),
                    &uivals->transform.xy);
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (transform_callback),
                    vals);
  g_object_set_data (G_OBJECT (scale), "update",
                     GINT_TO_POINTER (UPDATE_TRANSFORM));
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 4);
  button = gtk_button_new_from_stock (GIMP_STOCK_RESET);
  gtk_box_pack_end (GTK_BOX (hbox), button, FALSE, FALSE, 4);
  g_signal_connect (button, "clicked",
                    G_CALLBACK (transform_reset_callback),
                    NULL);

  return vbox;
}


static GtkWidget *
create_general_page (PreviewData  *preview,
                     ProgressData *pdata)
{
  GtkWidget *table;
  GtkWidget *table2;
  GtkWidget *check;
  GtkObject *scale;
  GtkWidget *sizeentry;
  GtkWidget *vbox;
  gint       row;

  FreeTypeVals *vals = preview->vals;

  table = gtk_table_new (3, 3, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (table), 6);
  gtk_table_set_col_spacings (GTK_TABLE (table), 6);
  gtk_table_set_row_spacings (GTK_TABLE (table), 6);

  row = 0;

  scale = gimp_scale_entry_new (GTK_TABLE (table), 0, row,
				_("Font Size:"),
				SCALE_WIDTH, SPIN_BUTTON_WIDTH,
				vals->size, 1, 1024, 1, 10, 1,
				TRUE, 0.0, 0.0,
				NULL, NULL);

  gimp_image_get_resolution (preview->image_ID, &image_xres, &image_yres);

  sizeentry = gimp_size_entry_new (0, vals->unit, "%a", TRUE, FALSE,
				   FALSE, SPIN_BUTTON_WIDTH,
				   GIMP_SIZE_ENTRY_UPDATE_SIZE);
  gimp_size_entry_add_field (GIMP_SIZE_ENTRY (sizeentry),
			     GIMP_SCALE_ENTRY_SPINBUTTON (scale), NULL);
  gimp_size_entry_set_resolution (GIMP_SIZE_ENTRY (sizeentry), 0,
				  image_yres, TRUE);
  gimp_size_entry_set_refval_boundaries (GIMP_SIZE_ENTRY (sizeentry), 0,
					 1, 1024);
  gimp_size_entry_set_value (GIMP_SIZE_ENTRY (sizeentry), 0, vals->size);
  gtk_table_attach (GTK_TABLE (table), sizeentry, 3, 4, row, row + 1,
		    GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL, 0, 0);
  row++;
  g_signal_connect (sizeentry, "value_changed",
                    G_CALLBACK (fontsize_callback),
                    &vals->size);
  g_signal_connect (sizeentry, "unit_changed",
                    G_CALLBACK (unit_callback),
                    &vals->unit);
  g_object_set_data (G_OBJECT (sizeentry), "update",
                     GINT_TO_POINTER (UPDATE_SIZE));
  g_signal_connect (sizeentry, "value_changed",
                    G_CALLBACK (auto_preview_callback),
                    preview);
  g_signal_connect (sizeentry, "unit_changed",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  scale = gimp_scale_entry_new (GTK_TABLE (table), 0, row++,
				_("Spacing:"),
				SCALE_WIDTH, SPIN_BUTTON_WIDTH,
				vals->spacing, -1000, 10000, 1, 10, 0,
				TRUE, 0.0, 0.0,
				NULL, NULL);
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (gimp_int_adjustment_update),
                    &vals->spacing);
  g_object_set_data (G_OBJECT (scale), "update",
                     GINT_TO_POINTER (UPDATE_LAYOUT));
  g_signal_connect (scale, "value_changed",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  table2 = gtk_table_new (3, 2, FALSE);
  gimp_table_attach_aligned (GTK_TABLE (table), 0, row++,
			     NULL, 1.0, 0.5,
			     table2, 3, FALSE);

  check = gtk_check_button_new_with_label (_("Antialiasing"));
  gtk_table_attach (GTK_TABLE (table2), check, 0, 1, 0, 1,
		    GTK_EXPAND | GTK_FILL, 0, 0, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
				vals->antialias);
  g_signal_connect (check, "toggled",
                    G_CALLBACK (gimp_toggle_button_update),
                    &vals->antialias);
  g_object_set_data (G_OBJECT (check), "update",
                     GINT_TO_POINTER (UPDATE_RENDERING));
  g_signal_connect (check, "toggled",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  check = gtk_check_button_new_with_label (_("Hinting"));
  gtk_table_attach (GTK_TABLE (table2), check, 0, 1, 1, 2,
		    GTK_EXPAND | GTK_FILL, 0, 0, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(check),
				vals->hinted);
  g_signal_connect (check, "toggled",
                    G_CALLBACK (gimp_toggle_button_update),
                    &vals->hinted);
  g_object_set_data (G_OBJECT (check), "update",
                     GINT_TO_POINTER (UPDATE_LAYOUT));
  g_signal_connect (check, "toggled",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  check = gtk_check_button_new_with_label (_("Kerning"));
  gtk_table_attach (GTK_TABLE (table2), check, 0, 1, 2, 3,
		    GTK_EXPAND | GTK_FILL, 0, 0, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(check),
				vals->kerning);
  g_signal_connect (check, "toggled",
                    G_CALLBACK (gimp_toggle_button_update),
                    &vals->kerning);
  g_object_set_data (G_OBJECT (check), "update",
                     GINT_TO_POINTER (UPDATE_LAYOUT));
  g_signal_connect (check, "toggled",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  check = gtk_check_button_new_with_label (_("Create Bezier Outline"));
  gtk_table_attach (GTK_TABLE (table2), check, 1, 2, 2, 3,
		    GTK_EXPAND | GTK_FILL, 0, 0, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON(check),
				vals->outline);
  g_signal_connect (check, "toggled",
                    G_CALLBACK (gimp_toggle_button_update),
                    &vals->outline);

  vbox = gtk_vbox_new (FALSE, 4);

  gtk_box_pack_start (GTK_BOX (vbox), table, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), pdata->pbar, FALSE, FALSE, 0);

  return vbox;
}



/*  Public functions  */

void
start_progress (ProgressData *pdata)
{
  if (!GTK_WIDGET_VISIBLE (pdata->pbar))
    gtk_widget_show (pdata->pbar);

  gtk_progress_set_value (GTK_PROGRESS (pdata->pbar), 0);

  while (gtk_events_pending ())
    gtk_main_iteration ();

  pdata->timer = gtk_timeout_add (5, progress_timeout, pdata->pbar);
}

void
stop_progress (ProgressData *pdata)
{
  gtk_timeout_remove (pdata->timer);
  pdata->timer = 0;

  gtk_widget_hide (pdata->pbar);
}

FontFace *
dialog (gint32          image_ID,
	FreeTypeVals   *vals,
	FreeTypeUIVals *ui)
{
  FontFace     *font_face = NULL;
  ProgressData *pdata     = NULL;
  PreviewData  *preview;

  GtkWidget *dlg;
  GtkWidget *frame;
  GtkWidget *table;
  GtkWidget *hbox;
  GtkWidget *hbox2;
  GtkWidget *vbox;
  GtkWidget *vbox2;
  GtkWidget *fontsel;
  GtkWidget *button;
  GtkWidget *scrollbar;
  GtkWidget *check;
  GtkWidget *notebook;
  GtkWidget *page;

  uivals = ui;

  preview = g_new0 (PreviewData, 1);
  preview->image_ID = image_ID;
  preview->vals     = vals;
  preview->left     = g_new0 (FT_Pos, 1);

  gimp_ui_init ("freetype", TRUE);

  dlg = gimp_dialog_new (_("FreeType Renderer"), "freetype",
                         NULL, 0,
                         gimp_standard_help_func, "plug-in-freetype",

			 _("About"),       RESPONSE_ABOUT,
			 _("Font Table"),  RESPONSE_TABLE,
			 _("Configure"),   RESPONSE_CONFIGURE,
			 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			 GTK_STOCK_OK,     GTK_RESPONSE_OK,

			 NULL);

  g_signal_connect (dlg, "response",
                    G_CALLBACK (response_callback),
                    NULL);
  g_signal_connect (dlg, "destroy",
                    G_CALLBACK (gtk_main_quit),
                    NULL);

  vbox = gtk_vbox_new (FALSE, 12);
  gtk_container_set_border_width (GTK_CONTAINER (vbox), 12);
  gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dlg)->vbox), vbox);

  paned = gtk_vpaned_new ();
  gtk_box_pack_start (GTK_BOX (vbox), paned, TRUE, TRUE, 0);

  g_signal_connect (paned, "destroy",
                    G_CALLBACK (gtk_widget_destroyed),
                    &paned);

  pdata = g_new0 (ProgressData, 1);
  pdata->timer = 0;
  pdata->pbar = gtk_progress_bar_new ();
  gtk_progress_set_activity_mode (GTK_PROGRESS (pdata->pbar), TRUE);
  g_object_weak_ref (G_OBJECT (pdata->pbar),
                     (GWeakNotify) g_free, pdata);

  fontsel = fontsel_new (pdata);
  gtk_widget_set_size_request (GTK_WIDGET (fontsel), -1, 120);
  gtk_paned_pack1 (GTK_PANED (paned), fontsel, TRUE, FALSE);
  g_object_set_data (G_OBJECT (fontsel), "update",
                     GINT_TO_POINTER (UPDATE_FACE));
  g_signal_connect (fontsel, "face_changed",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  vbox2 = gtk_vbox_new (FALSE, 6);
  gtk_paned_pack2 (GTK_PANED (paned), vbox2, TRUE, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (vbox2), 0);

  table = gtk_table_new (2, 2, FALSE);
  gtk_box_pack_start (GTK_BOX (vbox2), table, TRUE, TRUE, 0);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_IN);
  gtk_table_attach (GTK_TABLE (table), frame, 0, 1, 0, 1,
		    GTK_FILL | GTK_EXPAND, GTK_FILL | GTK_EXPAND, 0, 0);

  preview->preview = gtk_drawing_area_new ();
  gtk_drawing_area_size (GTK_DRAWING_AREA (preview->preview), 64, 64);
  gtk_container_add (GTK_CONTAINER (frame), preview->preview);

  preview->adj_x = gtk_adjustment_new (0, 0, 64, 1, 16, 64);
  scrollbar = gtk_hscrollbar_new (GTK_ADJUSTMENT (preview->adj_x));
  gtk_table_attach (GTK_TABLE (table), scrollbar, 0, 1, 1, 2,
		    GTK_FILL | GTK_EXPAND, 0, 0, 0);

  preview->adj_y = gtk_adjustment_new (0, 0, 64, 1, 16, 64);
  scrollbar = gtk_vscrollbar_new (GTK_ADJUSTMENT (preview->adj_y));
  gtk_table_attach (GTK_TABLE (table), scrollbar, 1, 2, 0, 1,
		    0, GTK_FILL | GTK_EXPAND, 0, 0);

  gtk_widget_set_events (preview->preview,
			 GDK_BUTTON_PRESS_MASK |
			 GDK_BUTTON_RELEASE_MASK |
			 GDK_BUTTON_MOTION_MASK |
			 GDK_POINTER_MOTION_HINT_MASK);
  g_signal_connect (preview->preview, "event",
                    G_CALLBACK (preview_events),
                    preview);
  g_signal_connect (preview->preview, "size_allocate",
                    G_CALLBACK (preview_size_changed),
                    preview);

  g_signal_connect (preview->adj_x, "value_changed",
                    G_CALLBACK (preview_callback),
                    preview);
  g_signal_connect (preview->adj_y, "value_changed",
                    G_CALLBACK (preview_callback),
                    preview);

  hbox = gtk_hbox_new (FALSE, 12);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox, FALSE, FALSE, 0);

  check = gtk_check_button_new_with_label (_("Dot for Dot"));
  gtk_box_pack_start (GTK_BOX (hbox), check, FALSE, FALSE, 0);
  g_signal_connect (check, "toggled",
                    G_CALLBACK (dot_for_dot_callback),
                    &dot_for_dot);
  g_object_set_data (G_OBJECT (check), "update",
                     GINT_TO_POINTER (UPDATE_TRANSFORM));
  g_signal_connect (check, "toggled",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  hbox2 = gtk_hbox_new (FALSE, 6);
  gtk_box_pack_start (GTK_BOX (hbox), hbox2, FALSE, FALSE, 0);

  g_object_set_data (G_OBJECT (check), "inverse_sensitive", hbox2);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check), dot_for_dot);

  button = gtk_button_new_from_stock (GTK_STOCK_ZOOM_IN);
  gtk_box_pack_start (GTK_BOX (hbox2), button, FALSE, FALSE, 0);
  g_signal_connect (button, "clicked",
                    G_CALLBACK (zoom_in_callback),
                    preview);
  g_object_set_data (G_OBJECT (button), "update",
                     GINT_TO_POINTER (UPDATE_TRANSFORM));
  g_signal_connect (button, "clicked",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  zoom_factor_label = gtk_label_new ("1:1");
  gtk_box_pack_start (GTK_BOX (hbox2), zoom_factor_label, FALSE, FALSE, 0);

  button = gtk_button_new_from_stock (GTK_STOCK_ZOOM_OUT);
  gtk_box_pack_start (GTK_BOX (hbox2), button, FALSE, FALSE, 0);
  g_signal_connect (button, "clicked",
                    G_CALLBACK (zoom_out_callback),
                    preview);
  g_object_set_data (G_OBJECT (button), "update",
                     GINT_TO_POINTER (UPDATE_TRANSFORM));
  g_signal_connect (button, "clicked",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  hbox2 = gtk_hbox_new (FALSE, 6);
  gtk_box_pack_end (GTK_BOX (hbox), hbox2, FALSE, FALSE, 0);

  button = gtk_button_new_with_label (_("Update Preview"));
  gtk_misc_set_padding (GTK_MISC (GTK_BIN (button)->child), 2, 0);
  gtk_box_pack_end (GTK_BOX (hbox2), button, FALSE, FALSE, 0);
  gtk_widget_set_sensitive (button, !auto_update_preview);
  g_signal_connect (button, "clicked",
                    G_CALLBACK (preview_callback),
                    preview);

  check = gtk_check_button_new_with_label (_("Auto"));
  g_object_set_data (G_OBJECT (check), "inverse_sensitive", button);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
				auto_update_preview);
  gtk_box_pack_end (GTK_BOX (hbox2), check, FALSE, FALSE, 0);
  g_signal_connect (check, "toggled",
                    G_CALLBACK (gimp_toggle_button_update),
                    &auto_update_preview);
  g_signal_connect (check, "toggled",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  preview->entry = gtk_entry_new_with_max_length (MAX_NUM_GLYPHS);
  gtk_box_pack_start (GTK_BOX (vbox), preview->entry, FALSE, FALSE, 0);
  gtk_entry_set_text (GTK_ENTRY (preview->entry), vals->text);
  g_signal_connect (preview->entry, "changed",
                    G_CALLBACK (text_callback),
                    &vals->text);
  g_object_set_data (G_OBJECT (preview->entry), "update",
                     GINT_TO_POINTER (UPDATE_TEXT));
  g_signal_connect (preview->entry, "changed",
                    G_CALLBACK (auto_preview_callback),
                    preview);

  notebook = gtk_notebook_new ();
  gtk_box_pack_start (GTK_BOX (vbox), notebook, FALSE, FALSE, 0);

  page = create_general_page (preview, pdata);
  gtk_notebook_append_page (GTK_NOTEBOOK (notebook),
			    page, gtk_label_new (_("General")));
  page = create_transform_page (preview);
  gtk_notebook_append_page (GTK_NOTEBOOK (notebook),
			    page,  gtk_label_new (_("Transformation")));
  gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook), 0);

  g_object_set_data (G_OBJECT (fontsel), "paned", paned);

  g_signal_connect_object (notebook, "switch-page",
                           G_CALLBACK (notebook_callback),
                           G_OBJECT (fontsel), 0);

  g_object_set_data (G_OBJECT (dlg), "font_face", &font_face);
  g_object_set_data (G_OBJECT (dlg), "fontsel", fontsel);
  g_object_set_data (G_OBJECT (dlg), "vals", vals);
  g_object_set_data (G_OBJECT (dlg), "preview_data", preview);

  gtk_widget_show_all (dlg);

  gtk_widget_set_sensitive (dlg, FALSE);
  fontsel_configure (fontsel, TRUE);
  fontsel_set_font_by_name (fontsel, ui->font_family, ui->font_style);
  gtk_widget_set_sensitive (dlg, TRUE);

  preview->update = UPDATE_FULL;
  preview_callback (NULL, preview);

  gtk_main ();

  g_free (preview->buffer);
  g_free (preview);

  return font_face;
}
