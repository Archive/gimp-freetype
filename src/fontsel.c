/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GIMP FreeType Plug-in
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *                     Jens Lautenbacher <jtl@gimp.org>
 *
 * This plug-in contains code taken from the freetype2 library and
 * its demos, which is licensed under The FreeType Project LICENSE.
 *  Copyright 1996-2000 by
 *   David Turner, Robert Wilhelm, and Werner Lemberg
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <glib.h>
#include <string.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>

#include "freetype-intl.h"
#include "freetype-types.h"

#include "fontsel.h"
#include "interface.h"
#include "main.h"


#define FONTPATH_TOKEN   "freetype-fontpath"
#ifdef G_OS_WIN32
#define DEFAULT_FONTPATH "C:\\Windows\\fonts"
#else
#define DEFAULT_FONTPATH "/usr/share/fonts"
#endif

typedef struct
{
  GtkWidget *fontsel;
  GTree     *family;
  gchar     *style_name;
} StyleSelect;


static void     fontsel_insert_face             (const FT_Face  face,
						 const gchar   *file_name,
						 const gint     face_index);

static void     fontsel_family_select_callback  (GtkCList       *clist,
						 gint            row,
						 gint            column,
						 GdkEventButton *event,
						 gpointer        data);
static void     fontsel_style_select_callback   (GtkCList       *clist,
						 gint            row,
						 gint            column,
						 GdkEventButton *event,
						 gpointer        data);

static gint     fontsel_family_list_insert      (gpointer        key,
						 gpointer        value,
						 gpointer        data);
static gint     fontsel_style_list_insert       (gpointer        key,
						 gpointer        value,
						 gpointer        data);
static gint     fontsel_remove_family           (gpointer        key,
						 gpointer        value,
						 gpointer        data);
static gint     fontsel_remove_face             (gpointer        key,
						 gpointer        value,
						 gpointer        data);

static gboolean fontsel_is_font_file            (const gchar    *name);
static void     fontsel_scan_directory          (const gchar    *path);

static gboolean fontsel_directories_dialog      (GtkWidget      *parent,
                                                 const gchar    *message,
					         gchar         **path);
static gint     fontsel_set_style_by_name       (StyleSelect    *select);


GTree    *families      = NULL;
FontFace *face_selected = NULL;


GtkWidget *
fontsel_new (ProgressData *pdata)
{
  GtkWidget *paned;
  GtkWidget *clist;
  GtkWidget *clist2;
  GtkWidget *scrolled_window;

  paned = gtk_hpaned_new ();

  g_signal_new ("face_changed",
                GTK_TYPE_WIDGET,
                G_SIGNAL_RUN_LAST,
                0,
                NULL, NULL,
                g_cclosure_marshal_VOID__VOID,
                G_TYPE_NONE,
                0,
                NULL);

  g_object_set_data (G_OBJECT (paned), "progress_data", pdata);

  /*  family  */
  clist = gtk_clist_new (1);
  gtk_clist_set_column_title (GTK_CLIST (clist), 0, _("Font Family"));
  gtk_clist_column_titles_show (GTK_CLIST (clist));
  gtk_clist_column_titles_passive (GTK_CLIST (clist));
  gtk_widget_show (clist);

  scrolled_window =
    gtk_scrolled_window_new (gtk_clist_get_hadjustment (GTK_CLIST (clist)),
			     gtk_clist_get_vadjustment (GTK_CLIST (clist)));
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_ALWAYS);
  gtk_container_add (GTK_CONTAINER (scrolled_window), clist);
  gtk_widget_show (scrolled_window);

  gtk_paned_pack1 (GTK_PANED (paned), scrolled_window, TRUE, FALSE);

  /*  style  */
  clist2 = gtk_clist_new (1);
  gtk_clist_set_column_title (GTK_CLIST (clist2), 0, _("Font Style"));
  gtk_clist_column_titles_show (GTK_CLIST (clist2));
  gtk_clist_column_titles_passive (GTK_CLIST (clist2));
  gtk_widget_show (clist2);

  scrolled_window =
    gtk_scrolled_window_new (gtk_clist_get_hadjustment (GTK_CLIST (clist2)),
			     gtk_clist_get_vadjustment (GTK_CLIST (clist2)));
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
                                  GTK_POLICY_AUTOMATIC,
                                  GTK_POLICY_ALWAYS);
  gtk_container_add (GTK_CONTAINER (scrolled_window), clist2);
  gtk_widget_show (scrolled_window);

  gtk_paned_pack2 (GTK_PANED (paned), scrolled_window, TRUE, FALSE);

  g_signal_connect (clist, "select_row",
                    G_CALLBACK (fontsel_family_select_callback),
                    clist2);
  g_signal_connect (clist2, "select_row",
                    G_CALLBACK (fontsel_style_select_callback),
                    paned);

  g_object_set_data (G_OBJECT (paned), "family_list", clist);
  g_object_set_data (G_OBJECT (paned), "style_list",  clist2);

  return paned;
}

void
fontsel_configure (GtkWidget *fontsel,
		   gboolean   init)
{
  gchar        *path;
  GList        *list;
  GList        *dirs;
  GtkWidget    *parent;
  GtkWidget    *clist;
  ProgressData *pdata;

  g_return_if_fail (fontsel != NULL);

  parent = gtk_widget_get_toplevel (fontsel);
  path   = gimp_gimprc_query (FONTPATH_TOKEN);

  if (init)
    {
      if (path == NULL || !*path)
	{
	  path = g_strdup (DEFAULT_FONTPATH);

	  fontsel_directories_dialog
            (parent, _("You seem to be running the FreeType plug-in for the "
                       "first time. You need to specify a list of folders "
                       "where font files can be found on your system."),
             &path);
	}
    }
  else
    {
      init = fontsel_directories_dialog (parent, NULL, &path);
    }

  if (init)
    {
      pdata = g_object_get_data (G_OBJECT (fontsel), "progress_data");
      start_progress (pdata);

      if (families)
	{
	  g_tree_foreach (families,
                          (GTraverseFunc) fontsel_remove_family,
                          NULL);
	  g_tree_destroy (families);
	}

      families = g_tree_new ((GCompareFunc)strcmp);

      dirs = gimp_path_parse (path, 128, TRUE, NULL);

      for (list = dirs; list; list = g_list_next (list))
        fontsel_scan_directory (list->data);

      clist = g_object_get_data (G_OBJECT (fontsel), "family_list");
      gtk_clist_freeze (GTK_CLIST (clist));
      gtk_clist_clear (GTK_CLIST (clist));
      g_tree_foreach (families,
                      (GTraverseFunc) fontsel_family_list_insert,
                      clist);
      gtk_clist_thaw (GTK_CLIST (clist));

      stop_progress (pdata);
    }
}

/* ugh, this is an ugly workaround until the fontselector
   becomes a proper widget */
void
fontsel_get_selected_entry (FontFace **font_face)
{
  g_return_if_fail (font_face != NULL);

  *font_face = face_selected;
}

void
fontsel_set_font_by_name (GtkWidget *fontsel,
			  gchar     *family_name,
			  gchar     *style_name)
{
  GTree       *family = NULL;
  GtkCList    *clist;
  gint         row;
  StyleSelect *style_select;

  g_return_if_fail (fontsel != NULL);

  if (family_name)
    {
      family = g_tree_lookup (families, family_name);
    }

  if (family)
    {
      clist = g_object_get_data (G_OBJECT (fontsel), "family_list");
      row = gtk_clist_find_row_from_data (GTK_CLIST (clist), family);
      gtk_clist_select_row (GTK_CLIST (clist), row, 0);

      if ( !(gtk_clist_row_is_visible (clist, row) & GTK_VISIBILITY_FULL))
	gtk_clist_moveto (clist, row, 0, 0.0, 0.0);

      if (style_name)
	{
	  style_select = g_new (StyleSelect, 1);
	  style_select->fontsel    = fontsel;
	  style_select->family     = family;
	  style_select->style_name = style_name;

	  gtk_idle_add ((GtkFunction)fontsel_set_style_by_name, style_select);
	}
    }
}

FontFace *
fontsel_get_font_by_name (gchar *family_name,
			  gchar *style_name)
{
  GTree *family = NULL;

  g_return_val_if_fail (families != NULL, NULL);
  g_return_val_if_fail (family_name != NULL && style_name != NULL, NULL);

  family = g_tree_lookup (families, family_name);
  if (!family)
    return NULL;

  return g_tree_lookup (family, style_name);
}


/* private functions */

static void
fontsel_family_select_callback (GtkCList       *clist,
				gint            row,
				gint            column,
				GdkEventButton *event,
				gpointer        data)
{
  FontFace  *font_face = NULL;
  GtkWidget *clist2    = (GtkWidget *) data;
  GTree     *family;
  gint       face_row  = 0;

  family = gtk_clist_get_row_data (clist, row);

  gtk_clist_freeze (GTK_CLIST (clist2));
  gtk_clist_clear (GTK_CLIST (clist2));
  g_tree_foreach (family,
                  (GTraverseFunc) fontsel_style_list_insert,
                  clist2);
  gtk_clist_thaw (GTK_CLIST (clist2));

  if (face_selected && face_selected->style_name)
    font_face = g_tree_lookup (family, face_selected->style_name);

  if (! font_face)
    font_face = g_tree_lookup (family, "Regular");

  if (! font_face)
    font_face = g_tree_lookup (family, "Roman");

  if (font_face)
    face_row = gtk_clist_find_row_from_data (GTK_CLIST (clist2), font_face);

  gtk_clist_select_row (GTK_CLIST (clist2), face_row, 0);

  if (! (gtk_clist_row_is_visible (GTK_CLIST (clist2), face_row) &
	 GTK_VISIBILITY_FULL))
    gtk_clist_moveto (clist, face_row, 0, 0.0, 0.0);
}

static void
fontsel_style_select_callback (GtkCList       *clist,
			       gint            row,
			       gint            column,
			       GdkEventButton *event,
			       gpointer        data)
{
  face_selected = gtk_clist_get_row_data (clist, row);

  g_signal_emit_by_name (G_OBJECT (data), "face_changed");
}

static gint
fontsel_family_list_insert (gpointer key,
			    gpointer value,
			    gpointer data)
{
  gint       row;
  gchar     *text[1];
  GtkWidget *clist = (GtkWidget *)data;

  text[0] = (gchar *)key;
  row = gtk_clist_append (GTK_CLIST (clist), text);
  gtk_clist_set_row_data (GTK_CLIST (clist), row, value);

  return FALSE;
}

static gint
fontsel_style_list_insert (gpointer key,
			   gpointer value,
			   gpointer data)
{
  gint       row;
  gchar     *text[1];
  GtkWidget *clist = (GtkWidget *)data;

  text[0] = (gchar *)key;
  row = gtk_clist_append (GTK_CLIST (clist), text);
  gtk_clist_set_row_data (GTK_CLIST (clist), row, value);

  return FALSE;
}

static void
fontsel_insert_face (const FT_Face  face,
		     const gchar   *file_name,
		     const gint     face_index)
{
  GTree    *family;
  FontFace *font;
  gchar    *name;

  family = g_tree_lookup (families, face->family_name);
  if (!family)
    {
      family = g_tree_new ((GCompareFunc) strcmp);
      name   = g_strdup (face->family_name);
      g_tree_insert (families, name, family);
    }

  font = g_new (FontFace, 1);
  font->file_name   = g_strdup (file_name);
  font->face_index  = face_index;
  font->family_name = g_strdup (face->family_name);

  if (face->style_name)
    name = g_strdup (face->style_name);
  else
    name = g_strdup ("(unknown)");

  font->style_name  = name;

  g_tree_insert (family, name, font);
}

static gint
fontsel_remove_family (gpointer key,
		       gpointer value,
		       gpointer data)
{
  GTree *family = (GTree *)value;

  g_free (key);
  g_tree_foreach (family,
                  (GTraverseFunc) fontsel_remove_face,
                  NULL);
  g_tree_destroy (family);

  return FALSE;
}

static gint
fontsel_remove_face (gpointer key,
		     gpointer value,
		     gpointer data)
{
  FontFace *font = (FontFace *) value;

  g_free (key);
  g_free (font->file_name);
  g_free (font->family_name);
  g_free (font);

  return FALSE;
}

static gboolean
fontsel_is_font_file (const gchar *name)
{
  gint  len;

  if (g_file_test (name, G_FILE_TEST_IS_REGULAR))
    {
      len = strlen (name);

      if (len > 4 &&
	  (g_ascii_strncasecmp (name + len - 4, ".pfa", 4) == 0 ||
	   g_ascii_strncasecmp (name + len - 4, ".pfb", 4) == 0 ||
	   g_ascii_strncasecmp (name + len - 4, ".ttf", 4) == 0))
	{
	  return TRUE;
	}
    }

  return FALSE;
}

static void
fontsel_scan_directory (const gchar *path)
{
  GDir        *dir;
  const gchar *name;
  gchar       *fullname;
  FT_Face      face;
  FT_Error     error;
  gint         i;

  dir = g_dir_open (path, 0, NULL);

  if (!dir)
    return;

  while ((name = g_dir_read_name (dir)) != NULL)
    {
      fullname = g_build_filename (path, name, NULL);

      if (fontsel_is_font_file (fullname))
        {
          error = FT_New_Face (library, fullname, 0, &face);

          if (!error)
            {
              if (face->face_flags & FT_FACE_FLAG_SCALABLE)
                fontsel_insert_face (face, fullname, 0);

              for (i = 1; i < face->num_faces; i++)
                {
                  FT_Done_Face (face);
                  error = FT_New_Face (library, fullname, i, &face);

                  if (!error)
                    {
                      if (face->face_flags & FT_FACE_FLAG_SCALABLE)
                        fontsel_insert_face (face, fullname, i);
                    }
                }

              FT_Done_Face (face);
            }
        }
      g_free (fullname);

      while (gtk_events_pending ())
        gtk_main_iteration ();
    }
  g_dir_close (dir);
}

static gboolean
fontsel_directories_dialog (GtkWidget    *parent,
                            const gchar  *message,
			    gchar       **path)
{
  GtkWidget *dialog;
  GtkWidget *path_editor;
  GtkWidget *label;
  gchar     *new_path = NULL;

  dialog = gimp_dialog_new (_("Configure Font Search Path"), "font-path",
                            parent,
                            GTK_DIALOG_DESTROY_WITH_PARENT,
                            gimp_standard_help_func, "plug-in-freetype",

			    GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			    GTK_STOCK_OK,     GTK_RESPONSE_OK,

			    NULL);

  if (message)
    {
      label = gtk_label_new (message);
      gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
      gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
      gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
      gtk_misc_set_padding (GTK_MISC (label), 12, 12);
      gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
			  label, FALSE, FALSE, 0);
    }

  path_editor = gimp_path_editor_new (_("Choose a folder"), *path);
  gtk_container_set_border_width (GTK_CONTAINER (path_editor), 12);
  gtk_widget_set_usize (GTK_WIDGET (path_editor), -1, 240);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
		      path_editor, TRUE, TRUE, 0);

  label = gtk_label_new (_("You may specify multiple folders here."));
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
  gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
  gtk_misc_set_padding (GTK_MISC (label), 12, 12);
  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
		      label, FALSE, FALSE, 0);

  gtk_widget_show_all (dialog);

  if (gimp_dialog_run (GIMP_DIALOG (dialog)) == GTK_RESPONSE_OK)
    new_path = gimp_path_editor_get_path (GIMP_PATH_EDITOR (path_editor));

  gtk_widget_destroy (dialog);

  if (new_path && (!*path ||
                   strcmp (*path, new_path) != 0 ||
                   strcmp (*path, DEFAULT_FONTPATH) == 0))
    {
      g_free (*path);
      *path = new_path;
      gimp_gimprc_set (FONTPATH_TOKEN, *path);

      return TRUE;
    }

  return FALSE;
}


/*  this is called from an idle loop  */

static gint
fontsel_set_style_by_name (StyleSelect *select)
{
  FontFace *font_face = NULL;
  GtkCList *clist;
  gint      row;

  g_return_val_if_fail (select != NULL, FALSE);

  font_face = g_tree_lookup (select->family, select->style_name);

  if (font_face)
    {
      clist = g_object_get_data (G_OBJECT (select->fontsel), "style_list");
      row = gtk_clist_find_row_from_data (GTK_CLIST (clist), font_face);
      gtk_clist_select_row (GTK_CLIST (clist), row, 0);

      if ( !(gtk_clist_row_is_visible (clist, row) & GTK_VISIBILITY_FULL))
	gtk_clist_moveto (clist, row, 0, 0.0, 0.0);
    }

  g_free (select);

  return FALSE;
}
