/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GIMP FreeType Plug-in
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *                     Jens Lautenbacher <jtl@gimp.org>
 * outline.c
 * Copyright (C) 2000  Andy Thomas <alt@gimp.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <libgimp/gimp.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H

#include "outline.h"


/* Test the theory out  */

#define DEBUG 0

/* Need to scan ahead on the list of flags
 */

/* Patterns we can get are:-
 * 11   -> mean straigth line.
 * 101  -> mid point off the curve. (conic control point)
 * 1001 -> middle of the two of points are on the curve. (cubic curve)
 */

typedef enum {
  ANCHOR     = 1,
  CONTROL    = 2,
  NEXT_CURVE = 3
} PType;

struct _pnts {
  PType      type;
  FT_F26Dot6 x;
  FT_F26Dot6 y;
};

typedef struct _pnts OutPoints;

struct _UserPntData
{
  FT_F26Dot6 xadv;
  FT_F26Dot6 yadv;
  FT_Vector  last_point;
  gint       newcurve;
  gint       count;
};

typedef struct _UserPntData UserPntData;

GSList *plist        = NULL;
gint    next_contour = FALSE;

/* C A C */
static void
gen_controlnew (FT_Vector *vec,
		gpointer   data)
{
  OutPoints   *p   = g_new0 (OutPoints, 1);
  UserPntData *adv = (UserPntData *)data;
  FT_F26Dot6   x1  = vec->x + adv->xadv;
  FT_F26Dot6   y1  = adv->yadv - vec->y;

  p->x = (x1);
  p->y = (y1);
  p->type = CONTROL;
  plist = g_slist_append (plist, p);
  adv->count++;

#if FT_DEBUG
  g_print ("TYPE: 2 X: %d.%d Y: %d.%d\n",
	   (x1)/64,
	   (((x1)&0x3f)*100)/64,
	   (y1)/64,
	   (((y1)&0x3f)*100)/64);
#endif /* FT_DEBUG */
}

static void
gen_anchornew (FT_Vector *vec,
	       gpointer   data)
{
  gint        type = 0;
  OutPoints   *p   = g_new0 (OutPoints, 1);
  UserPntData *adv = (UserPntData *)data;
  FT_F26Dot6   x1  = vec->x + adv->xadv;
  FT_F26Dot6   y1  = adv->yadv - vec->y;

  if (adv->newcurve)
    type = NEXT_CURVE;
  else
    type = ANCHOR;

  adv->newcurve = FALSE;

  p->x = (x1);
  p->y = (y1);
  p->type = type;
  plist = g_slist_append (plist, p);
  adv->count++;

#if FT_DEBUG
  g_print ("TYPE: %d X: %d.%d Y: %d.%d\n", type,
	   (x1)/64,
	   (((x1)&0x3f)*100)/64,
	   (y1)/64,
	   (((y1)&0x3f)*100)/64);
#endif /* FT_DEBUG */
}

static void
free_plist (void)
{
  GSList *tmp = plist;

  for (tmp = plist; tmp; tmp = g_slist_next (tmp))
    g_free (tmp->data);

  g_slist_free (plist);
  plist = NULL;
}

void
export_path (gint32  image_ID,
	     gchar  *name)
{
  GSList  *tmp         = plist;
  gdouble *path_points = g_new (gdouble, g_slist_length (plist) * 3);
  gint     offset      = 0;

  for (tmp = plist; tmp; tmp = g_slist_next (tmp))
    {
      OutPoints *p    = tmp->data;
      gint       type = p->type;
      gdouble    dp   = 0.0;

      /*  Seems to produce better results if we ignore the fractional part  */
      dp = ((gdouble)(p->x)) / 64;
      path_points[offset++] = dp;

      dp = ((gdouble)(p->y)) / 64;
      path_points[offset++] = dp;

      dp = type;
      path_points[offset++] = dp;
    }

  free_plist ();
  plist = NULL;

  gimp_path_set_points (image_ID,
			name,
			1,  /*  Only value allowed  */
			offset,
			path_points);
}


/*
 * Debug an outline array
 */

/* unused

static void
dump_pnts (FT_Outline outline)
{
  gint j;

  g_print ("\npoints = %d contors %d\n", outline.n_points, outline.n_contours);
  for (j = 0; j < outline.n_points; j++)
    g_print ("%02x  (%d.%d,%d.%d)\t %x\n",
	     j,
	     (gint)outline.points[j].x >> 6,
	     (gint)((outline.points[j].x & 0x3f)*100)/64,
	     (gint)outline.points[j].y >> 6,
	     (gint)((outline.points[j].y & 0x3f)*100)/64,
	     (gint)(outline.tags[j]) );
  for (j = 0; j < outline.n_contours; j++)
    g_print ("%02x  (%01hx)\n",j,(int)(outline.contours[j]));
  g_print ("\n");
}

*/

/*
 * Set up the last point in the user data area.
 * USed to generate a control point when a new segment is found.
 */

static void
setlastpoint (UserPntData *adv,
	      FT_Vector   *lpnt)
{
  adv->last_point.x = lpnt->x;
  adv->last_point.y = lpnt->y;
}

/*
 * When new curves (contours) are defined we must have a different number for
 * the starting control point. This flag lets us decide that the point is
 * really the start of a new contour.
 */

static void
setnewcurve (UserPntData *adv,
	     gint         newcurve)
{
  adv->newcurve = newcurve;
}


/*
 * Debug....
 */

static void
print266 (FT_Vector *pnt,
	  gchar     *msg,
	  void      *user)
{
#if FT_DEBUG
  UserPntData *adv = (UserPntData *)user;
  g_print ("%s Point (%d.%d,%d.%d)   lpnt(%d.%d,%d.%d)\n",
	   msg,
	   (gint)(pnt->x + adv->xadv) >> 6,
	   (gint)(((pnt->x + adv->xadv) & 0x3f) * 100) / 64,
	   (gint)(adv->yadv-pnt->y) >> 6,
	   (gint)(((adv->yadv-pnt->y) & 0x3f) * 100) / 64,
	   (gint)(adv->last_point.x + adv->xadv) >> 6,
	   (gint)(((adv->last_point.x + adv->xadv) & 0x3f) * 100) / 64,
	   (gint)(adv->yadv-adv->last_point.y) >> 6,
	   (gint)(((adv->yadv-adv->last_point.y) & 0x3f) * 100) / 64);
#endif /* FT_DEBUG */
}

/* Look at the freetype spec to find out whats going on here... */

static gint
moveto (FT_Vector  *to,
	gpointer    data)
{
  print266 (to, "moveto", data);
  setlastpoint ((UserPntData *)data, to);
  setnewcurve ((UserPntData *)data, next_contour);
  next_contour = TRUE;

  return 0;
}

static gint
lineto (FT_Vector *to,
	gpointer   data)
{
  UserPntData *ud = (UserPntData *)data;

  print266 (to, "lineto", data);
  gen_anchornew (&ud->last_point, ud);
  gen_controlnew (&ud->last_point, ud);
  gen_controlnew (to, ud);
  setlastpoint (ud, to);

  return 0;
}

static gint
conicto (FT_Vector *control,
	 FT_Vector *to,
	 gpointer   data)
{
  FT_Vector    PB, PC;
  UserPntData *ud      = (UserPntData *)data;

  print266 (control, "conic_control", data);
  print266 (to, "conicto", data);
  gen_anchornew (&ud->last_point, ud);

  PB.x = ((ud->last_point.x + 2 * control->x)) / 3;
  PB.y = ((ud->last_point.y + 2 * control->y)) / 3;

  PC.x = ((to->x + 2 * control->x)) / 3;
  PC.y = ((to->y + 2 * control->y)) / 3;

  gen_controlnew (&PB,ud);
  gen_controlnew (&PC,ud);

  setlastpoint ((UserPntData *)data, to);

  return 0;
}

static gint
cubicto (FT_Vector *control1,
	 FT_Vector *control2,
	 FT_Vector *to,
	 gpointer   data)
{
  UserPntData *ud = (UserPntData *)data;

  print266 (control1, "cubic_control1", data);
  print266 (control2, "cubic_control2", data);
  print266 (to, "cubic", data);
  gen_anchornew (&ud->last_point, ud);
  gen_controlnew (control1, ud);
  gen_controlnew (control2, ud);
  setlastpoint (ud, to);

  return 0;
}

void
walk_outline (FT_Outline outline,
	      FT_F26Dot6 xadv,
	      FT_F26Dot6 yadv)
{
  /* Walk the outline */
  UserPntData adv;

  FT_Outline_Funcs outlinefunc =
  {
    moveto,
    lineto,
    conicto,
    cubicto
  };

  adv.xadv         = xadv;
  adv.yadv         = yadv;
  adv.last_point.x = 0x7fffffff;
  adv.last_point.y = 0x7fffffff;
  adv.count        = 0;

  FT_Outline_Decompose (&outline, &outlinefunc, &adv);
}
