/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GIMP FreeType Plug-in
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *                     Jens Lautenbacher <jtl@gimp.org>
 *
 * This plug-in contains code taken from the freetype2 library and
 * its demos, which is licensed under The FreeType Project LICENSE.
 *  Copyright 1996-2000 by
 *   David Turner, Robert Wilhelm, and Werner Lemberg
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <libgimp/gimp.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_RENDER_H

#include "freetype-types.h"

#include "main.h"
#include "render.h"
#include "outline.h"


#define FLOOR(x)  ((x) & -64)
#define CEIL(x)   (((x) + 63) & -64)
#define TRUNC(x)  ((x) >> 6)


typedef struct _Glyph Glyph;

struct _Glyph
{
  FT_UInt        index;    /* glyph index in face      */
  FT_Vector      pos;      /* position of glyph origin */
  FT_Glyph       image;    /* glyph image              */
};


static Glyph      glyphs[MAX_NUM_GLYPHS];
static gint       num_glyphs   = 0;
static FT_Matrix  trans_matrix = { 65536.0, 0.0, 0.0, 65536.0};
static FT_Face    face         = NULL;
static gint       bpp          = 4;
static gint       offset_x     = 0;
static gint       offset_y     = 0;
static FT_BBox    bbox         = { 0, 0, 0, 0 };
static FT_Pos     top          = 0;
static FT_Pos     right        = 0;


static void   blit_glyph_to_drawable    (GimpDrawable    *drawable,
					 GimpPixelRgn    *pixel_rgn,
					 FT_Bitmap       *source,
					 gint             x_top,
					 gint             y_top,
					 gboolean         antialias);
static void   blit_outline              (FT_OutlineGlyph  vecglyph,
					 FT_Pos           x_top,
					 FT_Pos           y_top);
static void   compute_bbox              (void);
static void   layout_glyphs             (FreeTypeVals    *vals,
					 FT_Pos          *left);
static void   render_string             (GimpDrawable    *drawable,
					 guchar          *preview_buffer,
					 gint             preview_width,
					 gint             height,
					 gint             preview_x,
					 gint             preview_y,
					 GimpPixelRgn    *pixel_rgn,
					 FreeTypeVals    *vals);
static void   prepare_text              (const gchar     *string);
static void   prepare_transform         (FreeTypeVals    *vals);

static GimpDrawable * create_drawable   (gint32           image_ID,
					 gchar           *name,
					 gint             width,
					 gint             height,
					 GimpPixelRgn    *pixel_rgn);

void
blit_glyph_to_buffer (guchar    *buffer,
		      gint       buffer_width,
		      gint       buffer_height,
		      gint       preview_x,
		      gint       preview_y,
		      FT_Bitmap *source,
		      gint       x_top,
		      gint       y_top,
		      gboolean   antialias)
{
  gint    i, j;
  gint    x, y;
  gint    width;
  gint    pitch;
  gint    height;
  guchar *src;
  guchar *dest;

#ifdef FT_DEBUG
  g_print ("%d(%d) x %d @ %d x %d\n",
	   source->width, source->pitch, source->rows,
	   x_top, y_top);
#endif

  x_top -= preview_x;
  y_top -= preview_y;

  if (x_top + source->width < 0 || x_top >= buffer_width ||
      y_top + source->rows  < 0 || y_top >= buffer_height)
    return;

  width  = source->width;
  pitch  = antialias ? source->pitch : source->pitch << 3;

  height = source->rows;

  src  = (guchar *)source->buffer;

  if (antialias)
    {
      for (i = 0, y = y_top; i < pitch * height; i += pitch, y++)
	{
	  if (y < 0 || y >= buffer_height)
	    continue;

	  dest = buffer + buffer_width * y + x_top;

	  for (j = 0, x = x_top; j < width; j++, x++)
	    {
	      if (x < 0 || x >= buffer_width)
		{
		  dest++;
		  continue;
		}

	      *dest = MIN (*dest, 255 - src[i + j]);
	      dest++;
	    }
	}
    }
  else
    {
      for (i = 0, y = y_top; i < pitch * height; i += pitch, y++)
	{
	  if (y < 0 || y >= buffer_height)
	    continue;

	  dest = buffer + buffer_width * y + x_top;

	  for (j = 0, x = x_top; j < width; j++, x++)
	    {
	      if (x < 0 || x >= buffer_width)
		{
		  dest++;
		  continue;
		}

	      if (src[(i + j) >> 3] & (1 << (7 - ((i + j) % 8))))
		*dest = 0;
	      dest++;
	    }
	}
    }
}

static void
blit_glyph_to_drawable (GimpDrawable *drawable,
			GimpPixelRgn *pixel_rgn,
			FT_Bitmap    *source,
			gint          x_top,
			gint          y_top,
			gboolean      antialias)
{
  static guchar *rect = NULL;
  gint    i, j;
  gint    width;
  gint    pitch;
  gint    height;
  guchar *src;
  guchar *dest;

#ifdef FT_DEBUG
  g_print ("%d(%d) x %d @ %d x %d\n",
	   source->width, source->pitch, source->rows,
	   x_top, y_top);
#endif

  width  = source->width;
  pitch  = antialias ? source->pitch : source->pitch << 3;
  height = source->rows;

  if (width < 1 || height < 1)
    return;

  rect = g_realloc (rect, width * height * bpp);
  gimp_pixel_rgn_get_rect (pixel_rgn, rect,
			   x_top, y_top,
			   MIN (width, drawable->width - x_top), height);

  src  = (guchar *) source->buffer;
  dest = rect - 1;

  if (antialias)
    {
      for (i = 0; i < pitch * height; i = i + pitch)
	for (j = 0; j < width; j++)
	  {
	    dest += bpp;
	    *dest = MAX (*dest, src[i + j]);
	  }
    }
  else
    {
      for (i = 0; i < pitch * height; i = i + pitch)
	for (j = 0; j < width; j++)
	  {
	    dest += bpp;
	    if (src[(i + j) >> 3] & (1 << (7 - ((i + j) % 8))))
	      *dest = 255;
	  }
    }

  gimp_pixel_rgn_set_rect (pixel_rgn, rect,
			   x_top, y_top,
			   MIN (width, drawable->width - x_top), height);
}

static void
blit_outline (FT_OutlineGlyph  vecglyph,
	      FT_Pos           x_top,
	      FT_Pos           y_top)
{
  FT_Outline outline = vecglyph->outline;
  /* dump_pnts(outline); */
  walk_outline (outline, x_top << 6, y_top << 6);
}


/**************************************************************
 *
 *  Compute the dimension of a string of glyphs in pixels
 *
 */
static void
compute_bbox (void)
{
  Glyph     *glyph;
  FT_BBox    glyph_bbox;
  FT_Vector  nw, sw, ne, se;
  gint       n;

  g_return_if_fail (face != NULL);

  /*  Not the most elegant way and I'm not too content with the resulting
      bbox, but it gives the most pleasant results we could achieve so far:

      Compute a bbox of the untransformed string using the ascender, descender
      and the right value which is computed when laying out the glyphs and
      transform this bbox. We could stop here, but there are cases where glyphs
      might go beyond that area, so we loop through the glyphs and extend the
      box if necessary.
   */

  nw.x = 0;  nw.y =   FT_MulFix (face->ascender,
				 face->size->metrics.y_scale);
  sw.x = 0;  sw.y = - FT_MulFix (face->descender,
				 face->size->metrics.y_scale);
  ne.x = right;  ne.y = nw.y;
  se.x = right;  se.y = sw.y;

  FT_Vector_Transform (&nw, &trans_matrix);
  FT_Vector_Transform (&sw, &trans_matrix);
  FT_Vector_Transform (&ne, &trans_matrix);
  FT_Vector_Transform (&se, &trans_matrix);

  bbox.xMin = TRUNC (FLOOR (MIN (MIN (nw.x, sw.x), MIN (ne.x, se.x))));
  bbox.xMax = TRUNC (CEIL  (MAX (MAX (nw.x, sw.x), MAX (ne.x, se.x))));
  bbox.yMin = TRUNC (FLOOR (MIN (MIN (nw.y, sw.y), MIN (ne.y, se.y))));
  bbox.yMax = TRUNC (CEIL  (MAX (MAX (nw.y, sw.y), MAX (ne.y, se.y))));

  for (n = 0, glyph = glyphs; n < num_glyphs; n++, glyph++)
    {
      FT_Glyph_Get_CBox (glyph->image, ft_glyph_bbox_pixels, &glyph_bbox);

      if (glyph_bbox.xMin < bbox.xMin)
	bbox.xMin = glyph_bbox.xMin;

      if (glyph_bbox.yMin < bbox.yMin)
	bbox.yMin = glyph_bbox.yMin;

      if (glyph_bbox.xMax > bbox.xMax)
	bbox.xMax = glyph_bbox.xMax;

      if (glyph_bbox.yMax > bbox.yMax)
	bbox.yMax = glyph_bbox.yMax;
    }

  top = bbox.yMin + bbox.yMax;
}


/**************************************************************
 *
 *  Layout a string of glyphs
 *
 */
static void
layout_glyphs (FreeTypeVals *vals,
	       FT_Pos       *left)
{
  Glyph     *glyph;
  FT_Error   error;
  FT_Vector  kern;
  FT_Vector  vec;
  FT_Pos     origin_x;
  FT_Pos     prev_origin_x;
  FT_UInt    load_flags;
  FT_UInt    prev_index;
  gboolean   use_kerning;
  gint       n;

  g_return_if_fail (face != NULL);

  load_flags = FT_LOAD_DEFAULT;

  if (!vals->hinted)
    load_flags |= FT_LOAD_NO_HINTING;

  if (!vals->antialias)
    load_flags |= FT_LOAD_MONOCHROME;

  origin_x    = 0;
  prev_index  = 0;
  use_kerning = FT_HAS_KERNING (face);

  for (n = 0, glyph = glyphs; n < num_glyphs; n++, glyph++)
    {
      prev_origin_x = origin_x;

      /* compute glyph origin */
      if (use_kerning && vals->kerning)
	{
          if (prev_index)
	    {
	      FT_Get_Kerning (face, prev_index, glyph->index,
			      vals->hinted ? ft_kerning_default
			                   : ft_kerning_unfitted,
			      &kern);

	      origin_x += kern.x;
	    }
	  prev_index = glyph->index;
	}

      glyph->pos.x = origin_x;
      glyph->pos.y = 0;

      error = FT_Load_Glyph (face, glyph->index, load_flags);
      if (error)
	continue;

      /* clear existing image if there is one */
      if (glyph->image)
	{
	  FT_Done_Glyph (glyph->image);
	  glyph->image = NULL;
	}

      error = FT_Get_Glyph (face->glyph, &glyph->image);
      if (error)
	{
	   glyph->image = NULL;
	   continue;
	}

      /*  transform the glyph  */
      vec = glyph->pos;
      FT_Vector_Transform (&vec, &trans_matrix);
      FT_Glyph_Transform (glyph->image, &trans_matrix, &vec);

      origin_x += face->glyph->advance.x;

      /* work around the "sliding dot" problem */
      if (prev_origin_x > origin_x)
	origin_x = prev_origin_x;

      if (vals->spacing)
	{
	  FT_Pos space;

	  space = FT_MulFix (vals->spacing, face->size->metrics.x_scale);
	  if (vals->hinted)
	    space = (space + 32) & -64;
	  origin_x += space;
	}

      if (left)
	left[n + 1] = origin_x;
    }

  right = origin_x;
}


/**************************************************************
 *
 *  Renders a given glyph vector set
 *
 */
static void
render_string (GimpDrawable *drawable,
	       guchar       *preview_buffer,
	       gint          preview_width,
	       gint          preview_height,
	       gint          preview_x,
	       gint          preview_y,
	       GimpPixelRgn *pixel_rgn,
	       FreeTypeVals *vals)
{
  Glyph          *glyph;
  FT_BitmapGlyph  bitmap;
  FT_Bitmap      *source;
  FT_Error        error;
  FT_Pos          x_top;
  FT_Pos          y_top;
  gint            n;

  g_return_if_fail (face != NULL);
  g_return_if_fail (! (drawable != NULL && preview_buffer != NULL));

  for (n = 0, glyph = glyphs; n < num_glyphs; n++, glyph++)
    {
      if (!glyph->image)
	continue;

      if (preview_buffer || !vals->outline)
	{
	  if (glyph->image->format != ft_glyph_format_bitmap)
	    {
	      error = FT_Glyph_To_Bitmap (&glyph->image,
					  vals->antialias ? ft_render_mode_normal
					                  : ft_render_mode_mono,
					  FALSE,  /* no additional translation */
					  TRUE);  /* destroy copy in image     */
	      if (error)
		{
		  glyph->image = NULL;
		  continue;
		}
	    }

	  bitmap = (FT_BitmapGlyph)glyph->image;
	  source = &bitmap->bitmap;

	  x_top = bitmap->left - offset_x;
	  y_top = top - bitmap->top - offset_y;

	  if (preview_buffer)
	    blit_glyph_to_buffer (preview_buffer,
				  preview_width, preview_height,
				  preview_x, preview_y,
				  source,
				  x_top, y_top,
				  vals->antialias);
	  else
	    blit_glyph_to_drawable (drawable, pixel_rgn, source,
				    x_top, y_top,
				    vals->antialias);
	}
      else
	{
	  blit_outline ((FT_OutlineGlyph)glyph->image,
			- offset_x, top - offset_y);
	}
    }
}


static void
prepare_text (const gchar* string)
{
  Glyph       *glyph;
  const gchar *p;

  num_glyphs = 0;

  if (!string || strlen (string) == 0)
    return;

  g_return_if_fail (face != NULL);

  for (p = string, glyph = glyphs;
       *p && num_glyphs < MAX_NUM_GLYPHS;
       p = g_utf8_next_char (p), glyph++, num_glyphs++)
    {
      glyph->index = FT_Get_Char_Index (face, (FT_ULong) g_utf8_get_char (p));
    }
}


static void
prepare_transform (FreeTypeVals *vals)
{
  trans_matrix.xx = (FT_Fixed) (vals->transform.xx * 65536.0);
  trans_matrix.xy = (FT_Fixed) (vals->transform.xy * 65536.0);
  trans_matrix.yx = (FT_Fixed) (vals->transform.yx * 65536.0);
  trans_matrix.yy = (FT_Fixed) (vals->transform.yy * 65536.0);
}


static GimpDrawable *
create_drawable (gint32        image_ID,
		 gchar        *name,
		 gint          width,
		 gint          height,
		 GimpPixelRgn *pixel_rgn)
{
  GimpImageType  type;
  GimpDrawable  *drawable;
  gint32         layer_ID;
  guchar        *buf;
  guchar        *dest;
  GimpRGB        fg;
  guchar         r, g, b;
  gint           i;

  width  = CLAMP (width,  GIMP_MIN_IMAGE_SIZE, GIMP_MAX_IMAGE_SIZE);
  height = CLAMP (height, GIMP_MIN_IMAGE_SIZE, GIMP_MAX_IMAGE_SIZE);

  type = gimp_image_base_type (image_ID);
  switch (type)
    {
    case GIMP_RGB:
      type = GIMP_RGBA_IMAGE;
      bpp = 4;
      break;

    case GIMP_GRAY:
      type = GIMP_GRAYA_IMAGE;
      bpp = 2;
      break;

    default:
      g_assert_not_reached ();
    }

#ifdef FT_DEBUG
  g_print ("create_drawable: %d x %d\n", width, height);
#endif

  layer_ID = gimp_layer_new (image_ID,
			     name,
			     width,
			     height,
			     type,
			     100,
			     GIMP_NORMAL_MODE);

  gimp_image_add_layer (image_ID, layer_ID, 0);
  drawable = gimp_drawable_get (layer_ID);

  /* fill with fg color and clear transparency */
  gimp_context_get_foreground (&fg);
  buf = dest = g_new (guchar, width * height * bpp);

  gimp_rgb_get_uchar (&fg, &r, &g, &b);

  if (bpp == 4)
    {
      for (i = 0; i < width * height; i ++)
	{
	  *dest++ = r;
	  *dest++ = g;
	  *dest++ = b;
	  *dest++ = 0;
	}
    }
  else if (bpp == 2)
    {
      guchar intensity = gimp_rgb_intensity_uchar (&fg);

      for (i = 0; i < width * height; i++)
	{
	  *dest++ = intensity;
	  *dest++ = 0;
	}
    }
  else
    g_assert_not_reached ();

  /* Set the tile cache size */
  gimp_tile_cache_ntiles ((width / gimp_tile_width () + 1) *
			  (height / gimp_tile_height () + 1));

  gimp_pixel_rgn_init (pixel_rgn, drawable, 0, 0, width, height, TRUE, FALSE);
  gimp_pixel_rgn_set_rect (pixel_rgn, buf, 0, 0, width, height);

  g_free (buf);

  return drawable;
}


const FT_BBox *
render_prepare (gint32        image_ID,
		gboolean      preview,
		FT_Pos       *left,
		FreeTypeVals *vals,
		FontFace     *font_face,
		RenderUpdate  update)
{
  static gdouble  xres   = 0.0;
  static gdouble  yres   = 0.0;
  static gdouble  factor = 1.0;
  FT_Error        error;
  gint            points;

  if (update & UPDATE_FACE)
    {
      if (face)
	{
	  FT_Done_Face (face);
	  face = NULL;
	}

      error = FT_New_Face (library,
			   font_face->file_name,
			   font_face->face_index,
			   &face);
      if (error)
	{
	  g_message ("FT_New_Face returned %x", error);
	  return NULL;
	}

      if (! (face->face_flags & FT_FACE_FLAG_SCALABLE))
	{
	  g_message ("Sorry, font is not scalable.");
	  return NULL;
	}

      update = UPDATE_FULL;
    }

  if (update & UPDATE_SIZE)
    {
      /* on first preview call and on final render, get the image resolution */
      if (preview || xres == 0.0)
	{
	  if (image_ID != -1)
	    gimp_image_get_resolution (image_ID, &xres, &yres);
	  else
	    xres = yres = 72.0;

	  factor = gimp_unit_get_factor (GIMP_UNIT_POINT);
	}

      if (vals->unit == GIMP_UNIT_PIXEL)
	points = ROUND (vals->size *
			factor / yres *
			64.0);
      else
	points = ROUND (vals->size *
			factor / gimp_unit_get_factor (vals->unit) *
			64.0);

      error = FT_Set_Char_Size (face,
				points, points, (gint) yres, (gint) yres);
      if (error)
	{
	  g_message ("FT_Set_Char_Size returned %x", (gint) error);
	  return NULL;
	}
      update = UPDATE_FULL;
    }

  if (update & UPDATE_TEXT)
    prepare_text (vals->text);

  if (update & UPDATE_TRANSFORM)
    prepare_transform (vals);

  /*  on final rendering of outlines we need to reload the glyphs  */
  if (vals->outline && !preview)
    update = UPDATE_FULL;

  if (update) /* FALSE if only the preview offset changed */
    {
      layout_glyphs (vals, left);
      compute_bbox ();
    }

  return (const FT_BBox *) &bbox;
}

void
render (gint32            image_ID,
	guchar           *preview_buffer,
	gint              preview_width,
	gint              preview_height,
	gint              preview_x,
	gint              preview_y,
	FreeTypeVals     *vals,
	FreeTypeRetVals  *retvals,
	FontFace         *font_face)
{
  GimpDrawable   *drawable;
  GimpPixelRgn    pixel_rgn;

  if (!vals->text || !*vals->text)
    return;

  offset_x = bbox.xMin;
  offset_y = bbox.yMin;

  if (preview_buffer)
    {
      render_string (NULL, preview_buffer,
		     preview_width, preview_height, preview_x, preview_y,
		     NULL, vals);
    }
  else if (image_ID != -1)
    {
      if (vals->outline)
	{
	  render_string (NULL, NULL, 0, 0, 0, 0, NULL, vals);
	  export_path (image_ID, vals->text);
	}
      else
	{
	  drawable = create_drawable (image_ID,
				      vals->text,
				      bbox.xMax - bbox.xMin,
				      bbox.yMax - bbox.yMin,
				      &pixel_rgn);
	  retvals->layerid = drawable->drawable_id;

	  render_string (drawable, NULL, 0, 0, 0, 0, &pixel_rgn, vals);
	  gimp_drawable_flush (drawable);
	  gimp_drawable_update (drawable->drawable_id,
				0, 0, drawable->width, drawable->height);
	  gimp_drawable_detach (drawable);
	}
    }
}


void
init_glyphs (void)
{
  Glyph *glyph;
  gint   n;

  for (n = 0, glyph = glyphs; n < MAX_NUM_GLYPHS; n++, glyph++)
    glyph->image = NULL;
}

void
free_glyphs (void)
{
  Glyph *glyph;
  gint   n;

  for (n = 0, glyph = glyphs; n < MAX_NUM_GLYPHS; n++, glyph++)
    {
      if (!glyph->image)
	continue;

      FT_Done_Glyph (glyph->image);
      glyph->image = NULL;
    }
}
