/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * GIMP FreeType Plug-in
 * Copyright (C) 2000-2003  Sven Neumann <sven@gimp.org>
 *                          Jens Lautenbacher <jtl@gimp.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __MAIN_H__
#define __MAIN_H__


#define  BUF_SIZE  256

struct _FreeTypeVals
{
  gchar      font_file[BUF_SIZE];
  gdouble    size;
  GimpUnit   unit;
  Matrix2    transform;
  gboolean   kerning;
  gboolean   hinted;
  gboolean   antialias;
  gboolean   outline;
  gint       spacing;
  gchar      text[BUF_SIZE];
};

struct _FreeTypeRetVals
{
  gint       runstatus;
  gint	     layerid;
};

struct _FreeTypeUIVals
{
  gchar      font_family[BUF_SIZE];
  gchar      font_style[BUF_SIZE];
  Matrix2    transform;
  gdouble    rotation;
};

struct _FontFace
{
  gchar     *family_name;
  gchar     *style_name;
  gchar     *file_name;
  gint       face_index;
};

extern FT_Library  library;


#endif /* __MAIN_H__ */


