/* The GIMP -- an image manipulation program 
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis 
 * 
 * GIMP FreeType Plug-in
 * Copyright (C) 2000  Sven Neumann <sven@gimp.org>
 *                     Jens Lautenbacher <jtl@gimp.org>
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or 
 * (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program; if not, write to the Free Software 
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
 */

#ifndef __FONTSEL_H__
#define __FONTSEL_H__


/* the fontsel interface will change as soon as it becomes a proper widget */

GtkWidget * fontsel_new                (ProgressData  *pdata);
void        fontsel_configure          (GtkWidget     *fontsel,
					gboolean       init);
void        fontsel_get_selected_entry (FontFace     **font_face);
void        fontsel_set_font_by_name   (GtkWidget     *fontsel,
					gchar         *family_name,
					gchar         *style_name);
FontFace  * fontsel_get_font_by_name   (gchar         *family_name,
					gchar         *style_name);


#endif /* __FONTSEL_H__ */






